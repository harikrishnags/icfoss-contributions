import random
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client import InfluxDBClient, Point, WritePrecision
import paho.mqtt.client as mqtt
import json
from datetime import datetime, timezone, timedelta

url = "http://localhost:8086"
token = "tCNu_8aABDBrBnHqFcTUzqdY4IJSEsxIGx_6kumpj5hlllD7g3wbp4CBjZ0YT_THnFWfJR-DrTT4rly0tX_6zA=="
org = "NewOrg"
bucket = "MQTT"

# MQTT credentials
mqtt_username = ""
mqtt_password = ""
mqtt_address = ""
mqtt_port = 1883
mqtt_client = ""

# Create an InfluxDB client
client = InfluxDBClient(url=url, token=token, org=org)

# Create a write API instance
write_api = client.write_api(write_options=SYNCHRONOUS)


# Topic for displaying everything related to gateway events
gateway_up_topic = "gateway/+/event/up"

# Subscribe to all topics under "application"
application_topic = "application/#"

# Topic for displaying everything related to a specific APPLICATION_ID (eg:39)
application_individual_topic = "application/39/#"

# Topic for displaying only the uplink payloads for a specific APPLICATION_ID (eg:39)
application_individual_up_topic = "application/39/device/+/event/up"


def convert_to_ist(timestamp_str):
    # Convert string timestamp to datetime object
    timestamp_utc = datetime.fromisoformat(timestamp_str).replace(tzinfo=timezone.utc)
    
    # Convert to IST timezone
    timestamp_ist = timestamp_utc.astimezone(timezone(timedelta(hours=5, minutes=30)))
    
    # Format the timestamp in IST
    formatted_time = timestamp_ist.strftime('%Y-%m-%d %H:%M:%S')
    
    return formatted_time

def on_connect(client, userdata, flags, rc):
    """
    Callback function called when the client connects to the broker.
    """
    print("Connected with result code " + str(rc))

    # Subscribe to the desired MQTT topics
    client.subscribe(application_topic)

def on_message(client, userdata, msg):
    """
    Callback function called when a message is received.
    """
    try:
        # Decode the payload as JSON
        payload = json.loads(msg.payload.decode())
        print(payload)

        # Extract relevant data from the payload
        device_name = payload.get('deviceName')
        device_id = payload.get("devEUI")
        payload_fields = payload.get("object", {})

        # extract the time from payload    
        utc_time_str = payload['rxInfo'][0]['time']
    
        # convert utc time to ist
        ist_time = convert_to_ist(utc_time_str)
        
        print("Device Name:  ", device_name)
        print("Device ID:", device_id)
        print("Payload Fields:", payload_fields)
        print("Time (IST): ", ist_time)
        
        # Prepare data for InfluxDB
        influx_data = [
            {
                "measurement": "MQTT",
                "tags": {
                    "device_id": device_id,
                    "device_name": device_name
                },                
                "time": ist_time,
                "fields": payload_fields
            }
        ]

        # Send data to InfluxDB
        write_api.write(bucket=bucket, org=org, record=influx_data)

    except json.JSONDecodeError as e:
        print("Error decoding JSON:", str(e))
        return 

# Set up the InfluxDB client
client = InfluxDBClient(url="http://localhost:8086", token=token, org=org)
write_api = client.write_api(write_options=SYNCHRONOUS)

# Create an MQTT client instance
mqtt_client = mqtt.Client(mqtt_client)

# Set the username and password for authentication
mqtt_client.username_pw_set(mqtt_username, password=mqtt_password)

# Connect to the MQTT broker
mqtt_client.connect(mqtt_address, mqtt_port, 60)

# Set callback functions
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message

# Start the MQTT client loop (blocking)
mqtt_client.loop_forever()
