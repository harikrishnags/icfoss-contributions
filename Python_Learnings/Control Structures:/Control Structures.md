# Constructional Structures
***
## Conditional statements (if, else, elif).
 Python supports the usual logical conditions from mathematics:

* Equals: a == b
* Not Equals: a != b
* Less than: a < b
* Less than or equal to: a <= b
* Greater than: a > b
* Greater than or equal to: a >= b

These conditions can be used in several ways, most commonly in "if statements" and loops.
### If

An "if statement" is written by using the if keyword.

    a = 3
    b = 21
    if b > a:
      print("b is greater than a")

Python relies on indentation to define scope in the code. Other programming languages often use curly-brackets for this purpose.

    a = 33
    b = 200
    if b > a:
    print("b is greater than a") # you will get an error

### Nested If
You can have if statements inside if statements, this is called nested if statements.

      x = 41

      if x > 10:
      print("Above ten,")
      if x > 20:
         print("and also above 20!")
      else:
         print("but not above 20.")

      >>> Above ten,
          and also above 20!   
### Elif
The elif keyword is Python's way of saying "if the previous conditions were not true, then try this condition".

    a = 12
    b = 07
    if a < b:
       print ('A is greater than B')
    elif a==12:
       print("TRUE")

    >>> TRUE
### Else
The else keyword catches anything which isn't caught by the preceding conditions.

    a = 12
    b = 07
    if a < b:
       print ('A is greater than B')
    elif a==07:
       print("TRUE")
    else:
       print ('ERROR')
    
    >>> ERROR

##  Loops (for loops, while loops).
***
### for loops
* A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).

* This is less like the for keyword in other programming languages, and works more like an iterator method as found in other object-orientated programming languages.

* With the for loop we can execute a set of statements, once for each item in a list, tuple, set etc.

      fruits = ["apple", "banana", "cherry"]
      for x in fruits:
      print(x)
      if x == "banana":
         break
      
      >>> apple
          banana
* With the break statement we can stop the loop before it has looped through all the items.
* To loop through a set of code a specified number of times, we can use the range() function,
* The range() function returns a sequence of numbers, starting from 0 by default, and increments by 1 (by default), and ends at a specified number.

      for x in range(4):
          print(x)

      >>> 0
          1
          2
          3
* The range() function defaults to increment the sequence by 1, however it is possible to specify the increment value by adding a third parameter: range(0, 20, 3)
 
      for x in range(0,20,3):
         print(x)

      >>> 0
          0
          3
          6
          9
          12
          15
          18
* The else keyword in a for loop specifies a block of code to be executed when the loop is finished.

* Print all numbers from 0 to 4, and print a message when the loop has ended:  

      for x in range(5):
         print(x)
      else:
         print('loop is finished!')

      >>> 0
          1
          2
          3
          4
          loop is finished!
### Nested for loops
* A nested loop is a loop inside a loop.The "inner loop" will be executed one time for each iteration of the "outer loop".

      hardware = ['Tools', 'Machines']
      software = ['Program', 'security']
      for x in hardware:
         for y in software:
               print(x + " vs " + y)

      >>> Tools vs Program
          Tools vs security
          Machines vs Program
          Machines vs security
### While loops

With the while loop we can execute a set of statements as long as a condition is true.

      i = 1
      while i < 6:
      print(i)
      i += 1

      >>>1
         2
         3
         4
         5
### Break statement
* With the break statement we can stop the loop even if the while condition is true:

* Exit the loop when i is 3:

      i = 1
      while i < 6:
      print(i)
      if i == 3:
         break
      i += 1

      >>> 1
          2
          3

### Continue statement
* With the continue statement we can stop the current iteration, and continue with the next:

* Continue to the next iteration if i is 3:

      i = 0
      while i < 6:
      i += 1
      if i == 3:
         continue
      print(i)

      >>> 1
          2
          4
          5
          6








