# Modules and Packages
***
A Python module is a file that contains Python definitions and statements. A module can define functions, classes, and variables. A module can also include runnable code. Grouping related code into a module makes the code easier to find, reuse, and maintain. Modules can also be used to organize code into logical units, such as by functionality or by feature.

There are two types of modules in Python: built-in and user-defined. 
* Built-in modules are provided by the Python interpreter and are always available for use.
 * User-defined modules are created by programmers and are saved as .py files.

* To use a module in your Python code, you need to import it.

For example, to import the math module, you would use the following code:


        import math
To create a module just save the code you want in a file with the file extension .py

* // created a python file  - newmodule.py
        
        newmodule.py

        def greeting(name):
        print("Hello, " + name)

* Now we can use the module by using the import statement.

        import newmodule
        newmodule.greeting("Harikrishna")

        >>> Hello, Harikrishna

* Another example : variablesin modules

        newmodule.py

        sports = {
            "virat":"Cricket",
            "Neymar": "Football",
            "Kobe": "Basketball"
        }
* Import the module named mymodule, and access the sports dictionary:

        import newmodule
        
        new = newmodule.sports["virat"]
        print(new)

        >>> Cricket

##### Naming a module
We can name the module file whatever you like, but it must have the file extension " .py "
##### Renaming a module
You can create an alias when you import a module, by using the 'as' keyword:
Create an alias for newmodule called new1:

    import newmodule as new1

    a = mx.sports["Neymar"]
    print(a)

    >>> Football

### Built-in Modules
There are several built-in modules in Python, which you can import whenever you like.

Import and use the platform module

        import platform
        new = platform.system()
        print(new)

        >>> Linux

#### Using the dir() Function
There is a built-in function to list all the functions names ( or variable names) in a module. The dir() function

        import platform

        x = dir(platform)
        print(x)

        #### List all the defined names belonging to the platform module

* The dir() function can be used on all modules, also the ones you create yourself.

#### Import from module
You can choose to import only parts from a module, by using the from keyword.
        
newmodule.py

        def greeting(name):
        print("Hello, " + name)

        person1 = {
        "name": "John",
        "age": 36,
        "country": "Norway"
        }
Import only the person1 dictionary from the module

        from newmodule.py import person1
        print(person1["age"])

        >>> 36

### Python Packages
* Python modules may contain several classes, functions, variables, etc. whereas Python packages contain several modules. 
* In simpler terms, Package in Python is a folder that contains various modules as files.

##### Understanding __init__.py
* __init__.py helps the Python interpreter recognize the folder as a package. It also specifies the resources to be imported from the modules. 
* If the __init__.py is empty this means that all the functions of the modules will be imported. 
* We can also specify the functions from each module to be made available.
#### Creating Package
Let’s create a package in Python named mypckg that will contain two modules mod1 and mod2. 

To create this module follow the below steps:

* Create a folder named mypckg.
* Inside this folder create an empty Python file i.e. __init__.py
* Then create two modules mod1 and mod2 in this folder.

mod1.py

        def func():
            print("Hello World !")

mod2.py
        
        def sum(a,b):
            return a+b
For example, we can also create the __init__.py file for the above module as: 

__init__.py

        from .mod1 import func
        from .mod2 import sum

This __init__.py will only allow the func and sum functions from the mod1 and mod2 modules to be imported.

##### Import Modules from a Package

###### Syntax
        
        import package_name.module_name

###### Example 1
We will import the modules from the above-created package and will use the functions inside those modules

        from mypckg import mod1
        from mypckg import mod2
        
        mod1.func()   >>>> Hello World !
        
        
        add = mod2.sum(1, 2)
        print(add)    >>>> 3

## Python PIP
Python pip is a package-management system written in Python that is used to install and manage software packages. It is the standard package manager for Python.

* We can know the pip version through the terminal command

        pip --version

##### Downloading a package
Downloading a package is very easy.Open the command line interface and tell PIP to download the package you want.

Navigate your command line to the location of Python's script directory, and type the following:

        pip install camelcase

##### Using a Package
Once the package is installed, it is ready to use.
* Import the "camelcase" package into your project.


        Import and use "camelcase":

        import camelcase

        c = camelcase.CamelCase()

        txt = "hello world"

        print(c.hump(txt))

        >>>> Hello World
##### Remove a package
* To remove a package open the command line interface and use the uninstall command and followed by confirmation request.

        pip uninstall camelcase
##### List a package
* To List a package , open the command line interface and use the pip command.
        
        pip list

* Displays all the packags and their versions installed in your system.
