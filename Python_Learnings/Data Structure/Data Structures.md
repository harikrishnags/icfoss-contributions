# Data Structures
***
Data Structures are a way of organizing data so that it can be accessed more efficiently depending upon the situation. Data Structures are fundamentals of any programming language around which a program is built. Python helps to learn the fundamental of these data structures in a simpler way as compared to other programming languages.

### Lists
Python Lists are just like the arrays, declared in other languages which is an ordered collection of data. It is very flexible as the items in a list do not need to be of the same type.


    List = [1, 2,  3, "GFG", 2.3]
    print(List)

    >>>[1, 2, 3, 'GFG', 2.3]

#### Adding Elements in Lists
Adding the elements in the list can be achieved using the append(), extend() and insert() functions.

* The append() function adds all the elements passed to it as a single element.
* The extend() function adds the elements one-by-one into the list.
* The insert() function adds the element passed to the index value and increase the size of the list too.

       * my_list = [1, 2, 3]
        print(my_list)  >>> [1, 2, 3]
        
       * my_list.append([555, 12]) #add as a single element
        print(my_list) >>> [1, 2, 3, [555, 12]]
        
       * my_list.extend([234, 'more_example']) #add as different elements
        print(my_list) >>> [1, 2, 3, [555, 12],234, 'more_example']
        
       * my_list.insert(1, 'insert_example') #add element i
        print(my_list)  >>> [1,'insert_example', 2, 3, [555, 12],234, 'more_example']

#### Deleting Elements
* To delete elements, use the del keyword which is built-in into Python but this does not return anything back to us.
* If you want the element back, you use the pop() function which takes the index value.
* To remove an element by its value, you use the remove() function.

        my_list = [1, 2, 3, 'example', 3.132, 10, 30]
        del my_list[5] #delete element at index 5
        print(my_list)
        
        my_list.remove('example') #remove element with value
        print(my_list)
        
        a = my_list.pop(1) #pop element from list
        print('Popped Element: ', a, ' List remaining: ', my_list)
        my_list.clear() #empty the list
        print(my_list)

##### Output
        1, 2, 3, ‘example’, 3.132, 30]
        [1, 2, 3, 3.132, 30]
        Popped Element: 2 List remaining: [1, 3, 3.132, 30]
        []

##### You have several other functions that can be used when working with lists.

* The len() function returns to us the length of the list.
* The index() function finds the index value of value passed where it has been encountered the first time.
* The count() function finds the count of the value passed to it.
* The sorted() and sort() functions do the same thing, that is to sort the values of the list.  * The sorted() has a return type whereas the sort() modifies the original list.

        my_list = [1, 2, 3, 10, 30, 10]
        print(len(my_list))   >>> 6
        print(my_list.index(10))   >>> 3
        print(my_list.count(10))  >>> 2
        print(sorted(my_list)) >>> [1, 2, 3, 10, 10, 30]
        my_list.sort(reverse=True) 
        print(my_list)  >>> [30, 10, 10, 3, 2, 1]

### Dictionary
Dictionaries are used to store key-value pairs. 
*  To understand better, think of a phone directory where hundreds and thousands of names and their corresponding numbers have been added. 
* Now the constant values here are Name and the Phone Numbers which are called as the keys. 
* And the various names and phone numbers are the values that have been fed to the keys. 
* If you access the values of the keys, you will obtain all the names and phone numbers. So that is what a key-value pair is. And in Python, this structure is stored using Dictionaries. 

##### Changing and Adding key, value pairs
To change the values of the dictionary, you need to do that using the keys. So, you firstly access the key and then change the value accordingly. To add values, you simply just add another key-value pair as shown below.

        my_dict = {'First': 'Python', 'Second': 'Java'}
        print(my_dict)    >>> {'First': 'Python', 'Second': 'Java'}
        
        my_dict['Second'] = 'C++' #changing element
        print(my_dict)    >>> {'First': 'Python', 'Second': 'C++'}
        
        my_dict['Third'] = 'Ruby' #adding key-value pair
        print(my_dict)    >>> {‘First’: ‘Python’, ‘Second’: ‘C++’, ‘Third’: ‘Ruby’}

##### Deleting key, value pairs
* To delete the values, you use the pop() function which returns the value that has been deleted.
* To retrieve the key-value pair, you use the popitem() function which returns a tuple of the key and value.
* To clear the entire dictionary, you use the clear() function.

        my_dict = {'First': 'Python', 'Second': 'Java', 'Third': 'Ruby'}
        a = my_dict.pop('Third')  
        print('Value:', a)              >>> Value: Ruby
        print('Dictionary:', my_dict)   >>> Dictionary: {'First': 'Python', 'Second': 'Java'}
        b = my_dict.popitem()
        print('Key, value pair:', b)    >>> Key, value pair: (‘Second’, ‘Java’)
        print('Dictionary', my_dict)    >>> Dictionary {‘First’: ‘Python’}
        my_dict.clear() #empty dictionary
        print('n', my_dict)             >>> {}

##### Accessing Elements
You can access elements using the keys only. You can use either the get() function or just pass the key values and you will be retrieving the values.

        my_dict = {'First': 'Python', 'Second': 'Java'}
        print(my_dict['First'])       >>> Python
        print(my_dict.get('Second'))  >>> Java

### Tuple
Tuples are the same as lists are with the exception that the data once entered into the tuple cannot be changed no matter what. 

The only exception is when the data inside the tuple is mutable, only then the tuple data can be changed. 

You create a tuple using parenthesis or using the tuple() function.

        my_tuple = (1, 2, 3) 
        print(my_tuple)   >>> (1, 2, 3)

##### Accessing Elements
Accessing elements is the same as it is for accessing values in lists.

        my_tuple2 = (1, 2, 3, 'harikrishna') 
        for x in my_tuple2:
             print(x , end=" ")    >>> 1 2 3 'harikrishna'

        print(my_tuple2)           >>> (1, 2, 3, 'harikrishna') 
        print(my_tuple2[0])        >>>  1
        print(my_tuple2[:])        >>> (1, 2, 3, 'harikrishna') 
        print(my_tuple2[3][4])     >>> e
##### Appending Elements
To append the values, you use the ‘+’ operator which will take another tuple to be appended to it.

        my_tuple = (1, 2, 3)
        my_tuple = my_tuple + (4, 5, 6) #add elements
        print(my_tuple)     >>> (1, 2, 3, 4, 5, 6)
##### These functions are the same as they are for lists.


        my_tuple = (1, 2, 3, ['hindi', 'python'])
        my_tuple[3][0] = 'english'     
        print(my_tuple)             >>>> (1, 2, 3, ['english', 'python'])
        print(my_tuple.count(2))    >>>> 1
        print(my_tuple.index(['english', 'python']))     >>> 3
### Sets
Sets are a collection of unordered elements that are unique. Meaning that even if the data is repeated more than one time, it would be entered into the set only once. 

It resembles the sets that you have learnt in arithmetic. The operations also are the same as is with the arithmetic sets.

Sets are created using the flower braces but instead of adding key-value pairs, you just pass values to it.


        my_set = {1, 2, 3, 4, 5, 5, 5} 
        print(my_set)   >>> {1, 2, 3, 4, 5}

##### Adding elements
To add elements, you use the add() function and pass the value to it.


        my_set = {1, 2, 3}
        my_set.add(4) 
        print(my_set)  >>> {1, 2, 3, 4}
        

##### Operations in sets
The different operations on set such as union, intersection and so on are shown below.

        my_set = {1, 2, 3, 4}
        my_set_2 = {3, 4, 5, 6}
        print(my_set.union(my_set_2), '----------', my_set | my_set_2)  >>> {1, 2, 3 ,4, 5 , 6}---------- {1, 2, 3, 4, 5, 6}
        print(my_set.intersection(my_set_2), '----------', my_set & my_set_2)  >>> {3, 4}----------{3, 4}
        print(my_set.difference(my_set_2), '----------', my_set - my_set_2)  >>> {1, 2} ———- {1, 2}

        print(my_set.symmetric_difference(my_set_2), '----------', my_set ^ my_set_2)  >>>{1, 2, 5, 6} ———- {1, 2, 5, 6}

        my_set.clear()
        print(my_set) >>> set()
* The union() function combines the data present in both sets.
* The intersection() function finds the data present in both sets only.
* The difference() function deletes the data present in both and outputs data present only in the set passed.
* The symmetric_difference() does the same as the difference() function but outputs the data which is remaining in both sets.
