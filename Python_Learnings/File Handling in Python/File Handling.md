# File Handling in Python
***
File handling is an important part of any web application.Python has several functions for creating, reading, updating, and deleting files.

* The key function for working with files in python is open() function. The Open function take two parameters : filename and mode.
* There are four different methods (modes) for opening a file :
    
    * "r" - Read- Default value. Opens the file for reading, error if the files does not exist.
    * "a" - Append - Opens a files for appending, Creates the files if doesnot exist.
    * "w" - Write - Opens a file for writing, creates the files if doesnot exists.
    * "x" - Create-Creates the specified file, returns an error if the file already exists.
* In addition you can specify if the file should be handled as a binary or text mode.
    
    * "t"- Text - Default Value. Text Mode.
    * "b"- Binary - Binary mode (e.g Images)

### Syntax 
To Open a file for reading it is enough to specify the name of the file:
        
        f = open("newfile.txt")
                or 
        f = open("newfile.txt", "rt")
* Here "r" for read and "t" for text are Default values.

### Python File Open
* // creating a sample text file

    newfile.txt
    
            Hello! Welcome to my page.
            Myself Harikrishna GS.
            I'm a software developer.

* To open the file we have the in built open function.
* The open() function returns a file object, which has a read () method for reading the content of the file >>

        new = open("newfile.txt)
        print(new.read())

        >>> Output

        Hello! Welcome to my page.
        Myself Harikrishna GS.
        I'm a software developer.
* If the file is located in different location, we need to specify the path.

        new = open("D:\\samplefolder\newfile.txt", "r")
        print(new.read())


        >>>  Output

        Hello! Welcome to my page.
        Myself Harikrishna GS.
        I'm a software developer.

####  Read Only parts of the file.
By default the read() method returns the whole text, But you can specify how many characters you want to return.

* Return the 4 charaters of the file :
        
        new = open("newfile.txt", "r")
        print(new.read(4))

        >>> Hell
###  Read Lines

You can return one line by using the readline() method:

        new = open("newfile.txt","r")
        print(new.readline())

        >>> Hello! Welcome to my page.
* By looping through the line of file we can read the whole file

        new = open("newfile.txt")
        for file in new:
           print(file)
           
        >>> Output

        Hello! Welcome to my page.
        Myself Harikrishna GS.
        I'm a software developer.
### Close files

        new = open("newfile.txt","r")
        print(new.read())
        new.close()

## Python File Write
#### Writing to an existing file
To write in an existing file, you must add a parameter to the open() function.

* "a" -Append - will append to the end of the file.
* "w" -Write - will overwrite any existing content


        new = open("newfile.txt", "a")
        new.write(" Now I'm Working at ICFOSS")
        new.close()

        new = open("newfile.txt", "r")
        print(new.read())

Output
 
        Hello! Welcome to my page.
        Myself Harikrishna GS.
        I'm a software developer.  Now I'm Working at ICFOSS
#### To overwrite the content
        new = open("newfile.txt","w")
        new.write("The Whole content was deleted")
        new.close()

        new = open("newfile.txt","r")
        print(new.read())

Output
        
        The whole content was deleted 

### Create a New File
To Create a newfile in python, use the open() method, one of the following parameters;

* "x" - Create - creates a file , returns an error if the file exist.
* "a" - Append - will create a file if the specified file doesnot exist
* "w"- Write - will create a file if the specified file doesnot exists.
* // Creating a newfile - harinew.txt.
        
        new = open("harinew.txt", "x") 
        # Creates a newfile
* // Creating a newfile if it does not exist.

        new = open("harinew1.txt", "w")
        # Creates a newfile
### Python Delete file
To delete a file we need to import the OS module, and run os.remove() function.

* // Removing the newfile.txt file.
        
        import os
        os.remove("newfile.txt")
#### Check if the file exists
To avoid getting an error, we should check if the file exists before removing it

        import os
        if os.path.exists("newfile.txt"):
            os.remove("newfile.txt")
        else:
            print("The file doesnot exists")

#### Removing an entire folder
To remove an entire folder, use the os.rmdir() method;
* // Removing the folder "myfiles"
 
        import os
        os.rmdir("myfiles")

## Working with different filetypes
### CSV Files in Python
* CSV ( Comma Seperated value) is a simple file format used to store tabular data, such as a spreadsheet or database.
* A CSV file store tabular data (numbers and text) in plain text.Each line of the file is a data record.
* Each record consists of one or more fields, seperated by commas, comma is used as a field seperator.

### CSV file - Operations
* Reading a CSV File
* Writing to a CSV File
* Writing a dictionary to a CSV file

#### Reading a CSV file
While we could use the built-in open() function to work with CSV files in Python, there is a dedicated csv module that makes working with CSV files much easier.
* Create a csv file - datas.csv
datas.csv

        Name	 Age	Profession

        Hari	 22	Software Engineer
        Kavya  23     Electrical Engineer

* Lets read the file using csv.reader()

        import csv
        with open("datas.csv","r") as file:
           reader = csv.reader(file)
           for input in reader:
                print(input)

Output

    ['Name', 'Age', 'Profession']
    ['Hari', '22', 'Software Engineer']
    ['Kavya', '23', 'Electrical Engineer']

#### Writing a CSV file 

To Write to a CSV file in python, we can use the csv.writer() function.
The csv.writer() function return a writer object that converts the user data into a delimated string.
* This string can later be used to write into CSV files using the writerow() function.

        import csv
        with open('protagonist.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["SN", "Movie", "Protagonist"])
        writer.writerow([1, "Lord of the Rings", "Frodo Baggins"])
        writer.writerow([2, "Harry Potter", "Harry Potter"])
Output

        SN,Movie,Protagonist
        1,Lord of the Rings,Frodo Baggins
        2,Harry Potter,Harry Potter

#### Python CSV.DictReader() class
* The objects of csv.DictWriter() class can be used to write to a CSV file from a Python dictionary.
                
        csv.DictWriter(file, fieldnames)
* file - CSV file where we want to write to
* fieldnames - a list object which should contain the column headers specifying the order in which data should be written in the CSV file

        import csv

        with open('players.csv', 'w', newline='') as file:
        fieldnames = ['player_name', 'fide_rating']
        writer = csv.DictWriter(file, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerow({'player_name': 'Magnus Carlsen', 'fide_rating': 2870})
        writer.writerow({'player_name': 'Fabiano Caruana', 'fide_rating': 2822})
        writer.writerow({'player_name': 'Ding Liren', 'fide_rating': 2801})

Output

        player_name,fide_rating
        Magnus Carlsen,2870
        Fabiano Caruana,2822
        Ding Liren,2801
### JSON Files in Python

* The full form of JSON is JavaScript Object Notation. It means that a script (executable) file which is made of text in a programming language, is used to store and transfer the data. 

* Python supports JSON through a built-in package called JSON. To use this feature, we import the JSON package in Python script. 
* The text in JSON is done through quoted-string which contains the value in key-value mapping within { }.
* Import the JSON module
  
        import json

#### Parse JSON - Convert from JSON to Python
If you have a JSON string, you can parse it by using the json.loads() method.

The result will be a python dictionary.
        import json

        # some JSON:
        x =  '{ "name":"John", "age":30, "city":"New York"}'

        # parse x:
        y = json.loads(x)

        # the result is a Python dictionary:
        print(y["age"])

Output
        
    >>> 30
#### Convert from Python to JSON

If you have a Python object, you can convert it into a JSON string by using the json.dumps() method.

                import json

        # a Python object (dict):
        x = {
        "name": "John",
        "age": 30,
        "city": "New York"
        }

        # convert into JSON:
        y = json.dumps(x)

        # the result is a JSON string:
        print(y)
Output

        {"name": "John", "age": 30, "city": "New York"}
* Convert Python objects into JSON strings, and print the values:

        import json

        print(json.dumps({"name": "John", "age": 30}))
        print(json.dumps(["apple", "bananas"]))
        print(json.dumps(("apple", "bananas")))
        print(json.dumps("hello"))
        print(json.dumps(42))
        print(json.dumps(31.76))
        print(json.dumps(True))
        print(json.dumps(False))
        print(json.dumps(None))

Output

        {"name": "John", "age": 30}
        ["apple", "bananas"]
        ["apple", "bananas"]
        "hello"
        42
        31.76
        true
        false
        null

* Convert a Python object containing all the legal data types

        import json
        new = {
                "name": "Harikrishna",
                "age":"30",
                "married" : True,
                "parents_name": ("Sathi","Giri")
                "cars":[
                        {"model": "Audi A6","mpg": 26.25},
                        {"model":"Tesla", "mpg":24.1}
                ]
        }
        print(json.dumps(x))
Output

        {"name": "John", "age": 30, "married": true, "divorced": false, "children": ["Ann","Billy"], "pets": null, "cars": [{"model": "BMW 230", "mpg": 27.5}, {"model": "Ford Edge", "mpg": 24.1}]}
#### Format the Result

The json.dumps() method has parameters to make it easier to read the result:

Use the indent parameter to define the numbers of indents:

        json.dumps(x, indent=4)

* You can also define the separators, default value is (", ", ": "), which means using a comma and a space to separate each object, and a colon and a space to separate keys from values:

Use the separators parameter to change the default separator:

        json.dumps(x, indent=4, separators=(". ", " = "))
#### Order the Result
The json.dumps() method has parameters to order the keys in the result:

Use the sort_keys parameter to specify if the result should be sorted or not:

        json.dumps(x, indent=4, sort_keys=True)
