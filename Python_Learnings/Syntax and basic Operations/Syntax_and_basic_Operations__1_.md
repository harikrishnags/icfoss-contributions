# Python Fundamentals
***
##  Syntax and basic Operations
***
### Python Syntax
* Python was designed for readability, and has some similarities to the English language with influence from mathematics.
* Python uses new lines to complete a command, as opposed to other programming languages which often use semicolons or parentheses.
* Python relies on indentation, using whitespace, to define scope; such as the scope of loops, functions and classes. Other programming languages often use curly-brackets for this purpose.
* Python syntax can be executed by writing directly in the Command Line, for example:
        
        >>>print("Hello, World!")                  
        Hello, World!
***
###  Python indentation

* Indentation refers to the spaces at the beginning of a code line.

* Where in other programming languages the indentation in code is for readability only, the indentation in Python is very important.

* Python uses indentation to indicate a block of code.
        
        if 3 > 2:
            print(" Three is greater than two!")
* Otherwise Python will give you an error 
        
        if 5 > 2:
        print("Five is greater than two!")
        output will be : syntaxError

### Python Comments
* Python has commenting capability for the purpose of in-code documentation.

* Comments start with a #, and Python will render the rest of the line as a comment:

        #type your name
        print("Harikrishna")
* Comments can be used to explain Python code.
Comments can be used to make the code more readable.
* Comments can be used to prevent execution when testing code.
* You can add a multiline string (triple quotes) in your code, and place your comment inside it:

        """
        This is a comment
        written in
        more than just one line
        """
        print("Hey, Harikrishna")



### Variables
* Variables are containers for storing data values.
Python has no command for declaring a variable.
A variable is created the moment you first assign a value to it.

        x = 5
        y = "Jobin"
        print(x)
        print(y)
* Variables do not need to be declared with any particular type, and can even change type after they have been set.

        x = 4       # x is of type int
        x = "ICFOSS" # x is now of type str
        print(x)

* You can get the data type of a variable with the type() function.

        x = 5
        y = "John"
        print(type(x))
        print(type(y))       output : <class 'int'>
                                      <class 'str'>

##### Variable Names
* A variable can have a short name (like x and y) or a more descriptive name (age, carname, total_volume). 
 ###### Rules for Python variables:
* A variable name must start with a letter or the underscore character
* A variable name cannot start with a number
* A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
* Variable names are case-sensitive (age, Age and AGE are three different variables)
* Python allows you to assign values to multiple variables in one line:

        x, y, z = "IoT", "cybersecurity", "Drone"
        print(x)
        print(y)
        print(z)
 ###### Global Variables
* Variables that are created outside of a function are known as global variables.

* Global variables can be used by everyone, both inside of functions and outside.

        x = "awesome"
        def myfunc():
        print("Python is " + x)

        myfunc()  
        
        output will be : "Python is awesome"
### Data types

* In programming, data type is an important concept.Variables can store data of different types.

Python has the following data types built-in by default, in these categories:
* ##### Text Type:	str
* ##### Numeric Types:	int, float, complex
* ##### Sequence Types:	list, tuple, range
* ##### Mapping Type:	dict
* ##### Set Types:	set, frozenset
* ##### Boolean Type:	bool
* ##### Binary Types:	bytes, bytearray, memoryview
* ##### None Type:	NoneType

### Integers or Int & float 
* Int, or integer, is a whole number, positive or negative, without decimals, of unlimited length.

        y = 003
        z = -00700777

        print(type(z))
        print(type(y))

        output will be : <class 'int'>
                          <class 'int'>
* Float, or "floating point number" is a number, positive or negative, containing one or more decimals

        num1 = 1.10
        num2 = 1.0
        
        print(type(num1))

        output will be : 
        <class 'float'>
### Python strings
* Strings in python are surrounded by either single quotation marks, or double quotation marks.
* You can display a string literal with the print() function:

        print("Harikrishna")
        print('icfoss')
##### Multiline Strings
* You can assign a multiline string to a variable by using three quotes:

        a = """I'm Harikrishna Gs,
        I'm working as a software developer intern at International centre for free and open source software, 
        I'm 22 years old."""
        
        print(a)
##### Modifying Strings
* Python has a set of built-in methods that you can use on strings.
* The upper() method returns the string in upper case
* The lower() method returns the string in lower case

        a = "Hello, World!"
        print(a.upper())  >>> HELLO, WORLD
        print(a.lower())  >>> hello, world
* The replace() method replaces a string with another string.

        a = "Hello, World!"
        print(a.replace("H", "J")) >>> Jello,World
* The split() method returns a list where the text between the specified separator becomes the list items.

        a = "Hello, World!"
        print(a.split(",")) >>> ['Hello', ' World!']

##### Formatted strings
*  We can combine strings and numbers by using the format() method!

* The format() method takes the passed arguments, formats them, and places them in the string where the placeholders {} are

        age = 22
        txt = "My name is Harikrishna, and I am {}"
        print(txt.format(age))
        
        example 2
        
        quantity = 3
        item_no = 567
        price = 49.95
        myorder = "I want {} pieces of item {} for {} dollars."
        print(myorder.format(quantity, item_no, price))

### Lists
* Lists are used to store multiple items in a single variable.Lists are created using square brackets.

* Lists are one of 4 built-in data types in Python used to store collections of data, the other 3 are Tuple, Set, and Dictionary, all with different qualities and usage.

        fruits = ["apple", "banana", "cherry"]
        print(fruits)

        >>> ['apple', 'banana', 'cherry']
* List items are ordered, changeable, and allow duplicate values.

* List items are indexed, the first item has index [0], the second item has index [1] etc.

* Lists allow duplicate values:

        newlist = ["bab", "jay", ""sree, "alfy", "div"]
        print(list)

        >>> ["bab", "jay", ""sree, "alfy", "div"]

* You can specify a range of indexes by specifying where to start and where to end the range.

* When specifying a range, the return value will be a new list with the specified items.

        vavlist = ["apple", "banana", "cherry", "orange", "kiwi", "melon", "mango"]
        print(vavlist[2:5])

        >>> ["cherry", "orange", "kiwi"]

* To insert a new list item, without replacing any of the existing values, we can use the insert() method.

* The insert() method inserts an item at the specified index:

        thislist = ["apple", "banana", "cherry"]
        thislist.insert(2, "watermelon")
        print(thislist)

        >>> ["apple", "watermelon", "banana", "cherry"]
* To append elements from another list to the current list, use the extend() method.
* Add the elements of tropical to thislist:

        thislist = ["apple", "banana", "cherry"]
        tropical = ["mango", "pineapple", "papaya"]
        thislist.extend(tropical)
        print(thislist)

        >>> ["apple", "banana", "cherry", "mango", "pineapple", "papaya"]
### List Methods
Python has a set of built-in methods that you can use on lists.
* ###### append():	Adds an element at the end of the list
* ###### clear()	Removes all the elements from the list
* ###### copy()	Returns a copy of the list
* ###### count()	Returns the number of elements with the specified value
* ###### extend()	Add the elements of a list (or any iterable), to the end of the current list
* ###### index()	Returns the index of the first element with the specified value
* ###### insert()	Adds an element at the specified position
* ###### pop()	Removes the element at the specified position
* ###### remove()	Removes the item with the specified value
* ###### reverse()	Reverses the order of the list
* ###### sort()	Sorts the list
### Python Tuples
Tuples are used to store multiple items in a single variable.

* Tuple is one of 4 built-in data types in Python used to store collections of data, the other 3 are List, Set, and Dictionary, all with different qualities and usage.

* A tuple is a collection which is ordered and unchangeable.

*  Tuples are written with round brackets.
* Tuple items are ordered, unchangeable, and allow duplicate values.

* Tuple items are indexed, the first item has index [0], the second item has index [1] etc.

        thistuple = ("apple", "banana", "cherry", "apple", "cherry")
        print(thistuple)
### Tuple Methods
Python has two built-in methods that you can use on tuples.
* ##### count()	Returns the number of times a specified value occurs in a tuple
* ##### index()	Searches the tuple for a specified value and returns the position of where it was found
### Dictionary
* Dictionaries are used to store data values in key:value pairs.

*  A dictionary is a collection which is ordered, changeable and do not allow duplicates.
* Dictionaries are written with curly brackets, and have keys and values:



        profile = {
        "name": "Harikrishna",
        "age": 22,
        "designation": "Software developer"
        }
        print(profile)
* Dictionary items are ordered, changeable, and does not allow duplicates.

* Dictionary items are presented in key:value pairs, and can be referred to by using the key name.
* You can access the items of a dictionary by referring to its key name, inside square brackets:


        profile = {
        "name": "Harikrishna",
        "age": 22,
        "designation": "Software developer"
        }
        x = profile.get("name")

        >>> Harikrishna
### Dictionary Methods
Python has a set of built-in methods that you can use on dictionaries.
* ##### clear()	Removes all the elements from the dictionary
* ##### copy()	Returns a copy of the dictionary
* ##### fromkeys() Returns a dictionary with the specified keys and value
* ##### get()	Returns the value of the specified key
* ##### items()	Returns a list containing a tuple for each key value pair
* ##### keys()	Returns a list containing the dictionary's keys
* ##### pop()	Removes the element with the specified key
* ##### popitem()	Removes the last inserted key-value pair
* ##### setdefault()	Returns the value of the specified key. If the key does not exist: insert the key, with the specified value
* ##### update()	Updates the dictionary with the specified key-value pairs
* ##### values()	Returns a list of all the values in the dictionary

### Python Sets
* Sets are used to store multiple items in a single variable.

* Set is one of 4 built-in data types in Python used to store collections of data, the other 3 are List, Tuple, and Dictionary, all with different qualities and usage.

* A set is a collection which is unordered, unchangeable, and unindexed.

        new_set = {"app", "brew", "py"}
        print(new_set)

* Set items are unordered, unchangeable, and do not allow duplicate values.

* Unordered means that the items in a set do not have a defined order.

* Set items can appear in a different order every time you use them, and cannot be referred to by index or key.

* Set items are unchangeable, meaning that we cannot change the items after the set has been created.
* Duplicates are not allowed

        new_set = {"app", "brew", "py", "brew"}
                print(new_set)

        >>> {"app", "brew", "py"}

### Set methods

Python has a set of built-in methods that you can use on sets.


* ##### add()	Adds an element to the set
* ##### clear()	Removes all the elements from the set
* ##### copy()	Returns a copy of the set
* ##### difference()	Returns a set containing the difference between two or more sets
* ##### difference_update()	Removes the items in this set that are also included in another, specified set
* ##### discard()	Remove the specified item
* ##### intersection()	Returns a set, that is the intersection of two other sets
* ##### intersection_update()	Removes the items in this set that are not present in other, specified set(s)
* ##### isdisjoint()	Returns whether two sets have a intersection or not
* ##### issubset()	Returns whether another set contains this set or not
* ##### issuperset()	Returns whether this set contains another set or not
* ##### pop()	Removes an element from the set
* ##### remove()	Removes the specified element
* ##### symmetric_difference()	Returns a set with the symmetric differences of two sets
* ##### symmetric_difference_update()	inserts the symmetric differences from this set and another
* ##### union()	Return a set containing the union of sets
* ##### update()	Update the set with the union of this set and others
## Python Basic Operations
***
Python divides the operators in the following groups:

* Arithmetic operators
* Assignment operators
* Comparison operators
* Logical operators
* Identity operators
* Membership operators
* Bitwise operators

### Arithmetic operators
Arithmetic operators are used with numeric values to perform common mathematical operations:

     Operator        Name	        Example
        +	        Addition	x + y	
        -	        Subtraction	x - y	
        *	        Multiplication	x * y	
        /	        Division	x / y	
        %	        Modulus	        x % y	
        **	        Exponentiation	x ** y	
        //	        Floor division	x // y

### Logical operators
Logical operators are used to combine conditional statements:

        Operator	        Description	                                       Example	
        and 	Returns True if both statements are true	                x < 5 and  x < 10	
        or	        Returns True if one of the statements is true	                x < 5 or x < 4	
        not	        Reverse the result, returns False if the result is true	        not(x < 5 and x < 10)
### Comparison operators
Comparison operators are used to compare two values:

        Operator	Name	                        Example	
        ==	        Equal	                        x == y	
        !=	        Not equal	                x != y	
        >	        Greater than	                x > y	
        <	        Less than	                x < y	
        >=	        Greater than or equal to	x >= y	
        <=	        Less than or equal to	        x <= y









