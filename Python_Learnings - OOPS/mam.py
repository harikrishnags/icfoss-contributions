class Cats:
    def __init__(self, name, breed):
        self.name = name
        self.breed = breed

    def speak(self):
        print("{} is barking like a Dog". format(myCat.name))

myCat = Cats("Bavuma", "Persian")
print("My Cat's name is {} and its a {} Cat".format(myCat.name, myCat.breed))

myCat.speak()

# Python Inheritance example

class Animals:
    def __init__(self, name):
        self.name = name

    def speaks(self):
        pass

class Tiger(Animals):
    
    def speaks(self):
        return "Goooowrrr"
    
class Cow(Animals):
    
    def speaks(self):
        return " maaaaahhhhh"
    
myTiger = Tiger("Bindi")
myCow = Cow("Nandhi")

print("My tiger {} says {}".format(myTiger.name, myTiger.speaks()))
print("My cow {} says {}".format(myCow.name, myCow.speaks()))

 # example of  inheritance 
class Animal:
    def speak(self):
        pass

class Dog(Animal):
    def speak(self):
        return "Woof!"

class Cat(Animal):
    def speak(self):
        return "Meow!"

Dog_1 = Dog()
Cat_1 = Cat()

print("The dog says : {} ".format(Dog_1.speak()))


# Example of Polymorphism


class Dog:
    def speak(self):
        return "Woof!"

class Cat:
    def speak(self):
        return "Meow!"

def animal_sound(animal):
    return animal.speak()

myDog1= Dog()
myCat1= Cat()

print("Dogs barks like: {}".format(animal_sound(myDog1)))

