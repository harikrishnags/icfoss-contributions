# Python OOPs Concept
***
In Python,Object- Oriented Programming is a programming paradigm that uses objects and classes in programming. It aims to implement real world entities like inheritance, polymorphism, encapsulation, etc.

The main concept of OOPs is used to bind the data and functions that work on that together as a single unit so that no other part of the code can access the data.

### OOPs Concept in Python

* Class
* Objects
* Polymorphism
* Encapsulation
* inheritance
*  Data abstraction
### Python Class
A class is a collection of Objects. A class contains blueprints or prototype from which the objects are being created. It is a logical entity that contains some attributes and methods.
For Example:

Let's say you want to track the number of students in a class with their DOB. If a list is used , the first element would be the students name and the second element will be their DOB.
Lets suppose there are 100 students, then how would you know which element is supposed to be which ? What if we want other properties to these students? This lacks organsiation and its exact need for classes.

* Classes are created by keyword class.
* Attributes are the variables that belong to a class.
* Attributes are always public and can be accessed using the dot('.') operator.

#### Class - Syntax
    class ClassName:
    # Statement-1
    .
    .
    .
    # Statement-N

#### Creating an Empty class
 
    class Dog:
        pass
### Python Objects
Object is an entity that has state and behaviour associated with it.It may be a realworld object like computer, keyboard, book, pen, etc.

Integers, strings, floating-point numbers, even arrays, and dictionaries are all objects.Any single integer or single string is an object.

An object consists of :
* State : It is represented  by the attributes of an object. It also represents the propertiesof the object.
* Behaviour : It is represented by the methods of an object. It also reflects the response of an object to other objects.
* Identity: It gives a unique name to an object and enables one object to interact with other objects.
To understand the state, behavior, and identity let us take the example of the class dog (explained above). 
 
* The identity can be considered as the name of the dog.
* State or Attributes can be considered as the breed, age, or color of the dog.
* The behavior can be con* sidered as to whether the dog is eating or sleeping.

Here's a simple example of a class definition:

    class Dog:
        def __init__(self, name, breed):
            self.name = name
            self.breed = breed

        def bark(self):
            print(f"{self.name} is barking")

* Objects: An object is an instance of a class. It is a concrete realization of the class, with specific values for its attributes. You create objects from a class like this
 
       my_dog = Dog("Buddy", "Golden Retriever")


* Attributes: Attributes are variables that store data within an object. In the Dog class above, name and breed are attributes.

* Methods: Methods are functions defined within a class. They can operate on the attributes of the class and perform actions related to the class. In the Dog class, bark is a method.

* Constructor (init method): The __init__ method is a special method called a constructor. It is used to initialize the attributes of an object when it is created. It's the first method that gets called when you create an object. In the Dog class, __init__ initializes the name and breed attributes.

* Self: self is a reference to the instance of the object itself. It is the first parameter in every method of a class, and it is used to access the object's attributes and methods. You can name it differently, but self is a convention in Python.

Now, let's create an object of the Dog class and use its methods and attributes:

    my_dog = Dog("Buddy", "Golden Retriever")
    print(my_dog.name)  # Accessing the 'name' attribute
    my_dog.bark()  # Calling the 'bark' method

Output
 
    Buddy
    Buddy is barking

### Python Inheritance
Inheritance is the capability of one class to derive or inherit the properties from another class.

The class that derives properties is called the derived class or child class and the class from which the properties are being derived is called the base class or parent class.

The benifits of inheritance are:
* It represents real-world relationshps well.
* It provides the reusability of a code. We dont have to write the same code again and again. Also allows us to add more features to a class without modifying it.

#### Types of Inheritance
* Single Inheritance: Single-level inheritance enables a derived class to inherit characteristics from a single-parent class.
* Multilevel Inheritance: Multilevel inheritance enables a derived class to inherit properties from an immediate parent class which in turn inherits properties from his parent class.
* Hierarchical inheritance: Hierarchical level inheritance enables more than one derived class to inherit properties from a parent class.
* Multiple Inheritance: Multiple level inheritance enables one derived class to inherit properties from more than one base class.

Here is an example for Inheritance

    class Animal:
        def __init__(self, name):
            self.name = name

        def speak(self):
            pass

    class Dog(Animal):
        def speak(self):
            return "Woof!"

    class Cat(Animal):
        def speak(self):
            return "Meow!"

    my_dog = Dog("Buddy")
    my_cat = Cat("Whiskers")
    print(f"{my_dog.name} says: {my_dog.speak()}")
    print(f"{my_cat.name} says: {my_cat.speak()}")

Output

    Doggy says Woof
    Catty says Meow!

###  Python  Polymorphism

Polymorphism in Python refers to the ability of different objects to respond to the same method or attribute in a way that is appropriate for their specific type or class. This concept is closely tied to inheritance and method overriding. 
    
    class Car:
    def __init__(self, brand, mode)

    def move(self):
        print("Sail!")

    class Plane:
    def __init__(self, brand, model):
        self.brand = brand
        self.model = model

    def move(self):
        print("Fly!")

    car1 = Car("Ford", "Mustang")       #Create a Car class
    boat1 = Boat("Ibiza", "Touring 20") #Create a Boat class
    plane1 = Plane("Boeing", "747")     #Create a Plane class

    for x in (car1, boat1, plane1):
    x.move()

Output

    Drive!
    Sail!
    Fly!

Example -2

    class Circle:
        
        def area(self):
            return 3.14 * self.radius * self.radius

    class Rectangle(Shape):
        def __init__(self, width, height):
            self.width = width
            self.height = height

        def area(self):
            return self.width * self.height

    def calculate_area(shape):
        return shape.area()

    circle = Circle(5)
    rectangle = Rectangle(4, 6)

    print(f"Area of the circle: {calculate_area(circle)}")
    print(f"Area of the rectangle: {calculate_area(rectangle)}")

Output

    Area of the circle: 78.5
    Area of the rectangle: 24

Example - 3

    class Bird:

        def intro(self):
            print("There are many types of birds.")

        def flight(self):
            print("Most of the birds can fly but some cannot.")

    class sparrow(Bird):

        def flight(self):
            print("Sparrows can fly.")

    class ostrich(Bird):

        def flight(sef):
            print("There are many types of birds.")
lf):
            print("Ostriches cannot fly.")

    obj_bird = Bird()
    obj_spr = sparrow()
    obj_ost = ostrich()

    obj_bird.intro()
    obj_bird.flight()

    obj_spr.intro()
    obj_spr.flight()

    obj_ost.intro()
    obj_ost.flight()

Output

    There are many types of birds.
    Most of the birds can fly but some cannot.
    There are many types of birds.
    Sparrows can fly.
    There are many types of birds.
    Ostriches cannot fly.

##### What is the difference between polymorphism and inheritance?

Polymorphism and inheritance are both fundamental concepts in object-oriented programming, but they refer to different aspects of class relationships.

###### Inheritance:

* Inheritance is a mechanism that allows a class (called a subclass or derived class) to inherit properties and behaviors from another class (called a superclass or base class)
* The subclass can reuse and extend the functionalities of the superclass.

* Purpose: Inheritance promotes code reuse and establishes a hierarchy of classes. It allows the creation of a more specialized class (subclass) by inheriting characteristics from a more general class (superclass).

        class Animal:
            def speak(self):
                pass

        class Dog(Animal):
            def speak(self):
                return "Woof!"

        class Cat(Animal):
            def speak(self):
                return "Meow!"


###### Polymorphism

* Polymorphism refers to the ability of objects of different classes to respond to the same method or attribute name in a way that is appropriate for their specific type. 

* It allows a single interface (method or attribute) to be used with different types of objects.

Purpose: Polymorphism provides flexibility and allows code to work with objects of various types without knowing their specific classes. It allows for the development of more generic and reusable code.


        class Dog:
            def speak(self):
                return "Woof!"

        class Cat:
            def speak(self):
                return "Meow!"

        def animal_sound(animal):
            return animal.speak()
In this example, both Dog and Cat classes have a speak method. The animal_sound function can work with objects of either class, demonstrating polymorphic behavior.

* In summary, inheritance is a mechanism for creating a hierarchy of classes, while polymorphism is a concept that allows objects of different classes to be treated as objects of a common base class, responding to the same method or attribute name in a type-specific way. 
* Inheritance is a way to achieve polymorphism, but polymorphism can also be achieved without inheritance through interfaces.elf):
                return "Woof!"


### Python Encapsulation

Encapsulation is one of the fundamental concepts in Object oriented programming. 

It describes the idea of wrapping data and the methods that work on data within one unit.This puts restriction on accessing variables and methods directly and can prevent accidental modification of data.

* To prevent accidental change, an object’s variable can only be changed by an object’s method. Those types of variables are known as private variables.
* A class is an example of encapsulation as it encapsulates all the data that is member functions, variables, etc.

        class Person:
            def __init__(self, name, age):
                self.__name = name  # Private attribute
                self.__age = age    # Private attribute

            def get_name(self):
                return self.__name

            def set_name(self, new_name):
            # Additional validation can be added here
                self.__name = new_name

            def get_age(self):
                return self.__age

            def set_age(self, new_age):
            # Additional validation can be added here
            
                if 0 <= new_age <= 150:  # A simple age validation
                    self.__age = new_age
                else:
                    print("Invalid age")

            # Creating an instance of the Person class
        
        person = Person("Alice", 25)

            # Accessing private attributes using public methods
        
        print(f"Name: {person.get_name()}")
        print(f"Age: {person.get_age()}")

            # Modifying private attributes using public methods
        
        person.set_name("Alicia")
        person.set_age(30)

            # Trying to set an invalid age
        
        person.set_age(200)

            # Accessing the modified attributes
        
        print(f"Modified Name: {person.get_name()}")
        print(f"Modified Age: {person.get_age()}")
In this example:

* The Person class has private attributes __name and __age.
* Public methods get_name, set_name, get_age, and set_age are provided to access and modify these private attributes.
* The set_age method includes a simple validation to ensure that the age is within a reasonable range.
* By encapsulating the attributes within methods, we control how the outside world interacts with the Person class. 
* The internal representation (attributes and their validation) is hidden, and external code can only interact with the class through the defined methods. 
* This encapsulation helps in maintaining a clean and controlled interface, protecting the internal state, and allowing for future changes without affecting the external code that uses the class.
### Python Data Abstraction
Data Abstraction is a key concept in object-oriented programming (OOP) that involves hiding the complex implementation details of a class and exposing only the necessary features or behaviors to the outside world. 

It allows you to focus on what an object does rather than how it achieves its functionality. In Python, data abstraction is often achieved through encapsulation.



    class Car:
        def __init__(self, make, model, year):
            self.__make = make  # Private attribute
            self.__model = model  # Private attribute
            self.__year = year  # Private attribute

        def display_info(self):
            return f"{self.__year} {self.__make} {self.__model}"

    # Usage
    my_car = Car("Toyota", "Camry", 2022)
    print(my_car.display_info())

Here we used private attributes (prefixed with double underscores) to hide the internal details of the class, and you've provided a public method (display_info) to expose a simplified interface for interacting with instances of the class.

Output

    2022 Toyota Camry

* In this example, the Car class encapsulates the details of a car's make, model, and year. 
* The attributes are private, and access to them is controlled through the display_info method. 
* This way, the internal details of the Car class are abstracted, and external code interacts with a simplified interface.
* This demonstrates the encapsulation principle, where external code (in this case, the print statement) interacts with the Car class through the public method display_info, abstracting away the details of how the information is stored internally.


