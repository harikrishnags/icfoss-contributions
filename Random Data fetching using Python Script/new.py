from datetime import datetime
import random
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

# InfluxDB connection details
url = "http://localhost:8086"
token = "bRdmuksT3cW2Jlj9In6ZwbASr4JKvk8m_KOti9Fc020j1fD28AMEv-mRqPvJB0ze_gWbHJy4R37OndXzCv6XFg=="
org = "ICFOSS"
bucket = "Climate"


# Create an InfluxDB client
client = InfluxDBClient(url=url, token=token, org=org)

# Create a write API instance
write_api = client.write_api(write_options=SYNCHRONOUS)

# Function to generate random climate data with a random location
# Function to generate random climate data with a random location
def generate_random_climate_data():
    timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    temperature = round(random.uniform(10, 30), 2)  # Random temperature between 10 and 30 degrees Celsius
    humidity = round(random.uniform(30, 80), 2)     # Random humidity between 30% and 80%
    

    # Generate random latitude and longitude
    latitude = round(random.uniform(-90, 90), 6)
    longitude = round(random.uniform(-180, 180), 6)
    
    climate_data_point = (Point("climate_measurement")
                          .time(timestamp)
                          .field("temperature", temperature)
                          .field("humidity", humidity))
                          

    # Set tags individually
    climate_data_point = climate_data_point.tag("latitude", latitude)
    climate_data_point = climate_data_point.tag("longitude", longitude)

    return climate_data_point

# Number of climate data points to insert
num_points = 10

# Write random climate data to InfluxDB
for _ in range(num_points):
    climate_data_point = generate_random_climate_data()
    write_api.write(bucket=bucket, org=org, record=climate_data_point)

# Close the client
client.close()