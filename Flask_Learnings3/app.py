from flask import Flask, render_template, request, redirect, url_for, session, flash
import sqlite3

app = Flask(__name__)
app.secret_key = 'your_secret_key'  # Change this to a random, secure key

# SQLite database setup
db = sqlite3.connect('database.db')
db.execute('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username TEXT, password TEXT)')
db.commit()
db.close()

@app.route('/')
def home():
    return redirect (url_for('signup'))

@app.route('/profile')
def profile():
    return 'Welcome Home'

   
@app.route('/signup', methods=['GET','POST'])
def signup():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = sqlite3.connect('database.db')
        cursor = db.cursor()
        cursor.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, password))
        db.commit()
        db.close()
        flash('Signup successful', 'success')
        return redirect(url_for('login'))
    return render_template('signup.html')

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = sqlite3.connect('database.db')
        cursor = db.cursor()
        cursor.execute('SELECT * FROM users WHERE username = ? AND password = ?', (username, password))
        user = cursor.fetchone()
        db.close()
        if user:
            session['username'] = username
            flash('Login successful', 'success')
            return redirect(url_for('profile'))
        else:
            flash('Login failed. Check your credentials.', 'danger')
            return redirect(url_for('login'))
    return render_template ('login.html')
    
    

if __name__ == '__main__':
    app.run(debug=True)
