# Signup,Login,User-authentication,redirecting using html,css and Flask with database.
***
* Created a app.py file as the main python file, Created two directories - templates for (signup.html, login.html) and static for (style.css) and added a database.
* Installed flask through the terminal command -pip install flask.
* Installed sqlite3 -database using terminal command- * sudo apt -get update ,sudo apt -get install sqlite3.

### Creating signup and login html templates and styling it.
***

* Created login and signup using html forms for the user input and accessed to server using http POST method.
* Also added a link attribute to integrate with the style.css file.


### Creating the flask file app.py
***
* Imported Flask,render_template,request, redirect, url_for, flash.
* Imported sqlite3
###### Steps include:
* Created a flask instance in app and added a secret_key.
*       db = sqlite3.connect('database.db')
        db.execute('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, username TEXT, password TEXT)')
        db.commit()
        db.close()
    These lines handle the setup of an SQLite database. It connects to a database file named "database.db," creates a "users" table if it doesn't already exist, and then commits the changes to the database before closing the connection.
*       @app.route('/')
        def home():
        return redirect (url_for('signup'))

    Created a home function for the route of server and returned it into the signup page.
*       @app.route('/signup', methods=['POST'])
        def signup():
        if request.method == 'POST':

    This is a Flask route function that listens for HTTP POST requests at the "/signup" endpoint. condition that if the method is "POST" these lines extract the values of the username and password fields from the form data submitted by the user. The request object is provided by Flask and allows you to access data submitted in an HTTP request.
*       username = request.form['username']
        password = request.form['password']

    These lines extract the values of the username and password fields from the form data submitted in the POST request. The request object is provided by Flask and allows you to access data submitted by the client.
*       db = sqlite3.connect('database.db')
        cursor = db.cursor()
    You establish a connection to an SQLite database named "database.db" and create a cursor. The cursor is used to execute SQL statements on the database.              
*       cursor = db.cursor(): 
    This line creates a cursor object associated with the database connection. The cursor is used to execute SQL statements and fetch data from the database.   
*       cursor.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, password)) 
    This line inserts a new record into the "users" table in the SQLite database. It uses parameterized queries to safely insert the username and password values into the SQL statement, preventing SQL injection. The data is taken from the username and password variables.
*        db.commit()
    After executing the SQL statement, you commit the changes to the database. In SQLite, changes are not saved until you explicitly commit them.
* Similarly login fuction is called and routed to "/login" and It handles a POST request where a user submits their login credentials (username and password) and attempts to authenticate the user by querying an SQLite database.
*       username = request.form['username']
        password = request.form['password']

    These lines extract the values of the           username and password fields from the form data submitted in the POST request. The request object is provided by Flask and allows you to access data submitted by the client.
*       db = sqlite3.connect('database.db')
        cursor = db.cursor()
    You establish a connection to an SQLite database named "database.db" and create a cursor. The cursor is used to execute SQL statements on the database.
*       cursor.execute('SELECT * FROM users WHERE username = ? AND password = ?', (username, password))
        user = cursor.fetchone()
        db.close()

    These lines execute a SELECT query on the database to retrieve a user record where the provided username and password match the values stored in the database. The ? placeholders are used for safely inserting user inputs into the SQL query.
* user = cursor.fetchone()

    After executing the query, the fetchone() method is used to retrieve the first row (if any) that matches the query conditions. The result is stored in the user variable. If no match is found, user will be None. If a matching user is found in the database, user will be a tuple containing the user's data.

* If a matching user is found in the database the code sets the username in the Flask session, flashes a success message, and redirects the user to the "profile" page.
*       if __name__ == '__main__':
        app.run(debug=True)
    Finally, the code block at the end starts the Flask application and runs it with the debug mode enabled.









