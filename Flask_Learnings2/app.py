from flask import Flask, render_template, request, redirect, url_for, flash




app = Flask(__name__)
app.secret_key = '1234567'  


# User authentication (simplified, without a database)
users = {'user1': 'password1', 'user2': 'password2'}

@app.route('/')
def home():
    return 'Welcome to the Home Page!'

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username in users:
            flash("Username already exists.", "error")
        else:
            users[username] = password
            flash("Sign up successful!", "success")
            return redirect(url_for("login"))
    return render_template('Register.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if username in users and users[username] == password:
            flash('Login successful!', 'success')
            return redirect(url_for("home"))
        else:
            flash("Login unsuccessful. Please check your username and password.", "error")
    return render_template('login.html')

if __name__ == '__main__':
    app.run(debug=True)
