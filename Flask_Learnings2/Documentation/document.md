# User Authentication,Signin,Signup and Redirection using Flask and html.
***
* At first flask is installed using the command: # pip install flask.
* Created two directories namely "Flask_learnings" for the main flask application (app.py) and templates for the html files(register.html and login.html).
 #### 1. Created a signup and login form using html.
***
* Cretaed a signup form in Register.html using the html forms and form methods.
* Also Cretaed a login form with the same.
(check Register.html and login.html for detailed review).
### 2. Created a flask application app.py.
***
* from flask I've imported Flask,render_template,request,redirect,flash,url_for.
* Flask: This is the main Flask class that you use to create a Flask application.
* render_template: This function is used to render HTML templates in response to user requests. It's essential for displaying web pages to users.
* request: The request object is used to access data submitted in HTTP requests, such as form data, query parameters, and headers.
* redirect: This function is used to perform HTTP redirects, which send the user to a different URL or route.
* url_for: This function generates a URL for a specific route in your Flask application, making it easier to handle routes and ensure that your code is robust to changes in the URL structure.
* flash: The flash function is used to store and retrieve session-specific messages, such as success or error messages. It's often used for providing feedback to users after certain actions.
* The process involves:                          
Cretaed an instance of flask application and also routed it into the rootpath.Also setted a route for signup and login.
* By using request , imported the username and and password from the form fileds.
* return the values from Register.html and login.html using render_template.
* run the app instance as the main function.
*Check app.py in the Flask_learnings for detailed review.

