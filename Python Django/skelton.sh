harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ sudo apt install python3
[sudo] password for harikrishna: 
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
python3 is already the newest version (3.10.6-1~22.04).
The following packages were automatically installed and are no longer required:
  libflashrom1 libftdi1-2 libllvm13
Use 'sudo apt autoremove' to remove them.
0 upgraded, 0 newly installed, 0 to remove and 9 not upgraded.


harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ pip3 -v

Usage:   
  pip3 <command> [options]

Commands:
  install                     Install packages.
  download                    Download packages.
  uninstall                   Uninstall packages.
  freeze                      Output installed packages in requirements format.
  list                        List installed packages.
  show                        Show information about installed packages.
  check                       Verify installed packages have compatible dependencies.
  config                      Manage local and global configuration.
  search                      Search PyPI for packages.
  cache                       Inspect and manage pip's wheel cache.
  index                       Inspect information available from package indexes.
  wheel                       Build wheels from your requirements.
  hash                        Compute hashes of package archives.
  completion                  A helper command used for command completion.
  debug                       Show information useful for debugging.
  help                        Show help for commands.

General Options:
  -h, --help                  Show help.
  --debug                     Let unhandled exceptions propagate outside the main subroutine, instead of logging them to stderr.
  --isolated                  Run pip in an isolated mode, ignoring environment variables and user configuration.
  --require-virtualenv        Allow pip to only run in a virtual environment; exit with an error otherwise.
  -v, --verbose               Give more output. Option is additive, and can be used up to 3 times.
  -V, --version               Show version and exit.
  -q, --quiet                 Give less output. Option is additive, and can be used up to 3 times (corresponding to WARNING,
                              ERROR, and CRITICAL logging levels).
  --log <path>                Path to a verbose appending log.
  --no-input                  Disable prompting for input.
  --proxy <proxy>             Specify a proxy in the form [user:passwd@]proxy.server:port.
  --retries <retries>         Maximum number of retries each connection should attempt (default 5 times).
  --timeout <sec>             Set the socket timeout (default 15 seconds).
  --exists-action <action>    Default action when a path already exists: (s)witch, (i)gnore, (w)ipe, (b)ackup, (a)bort.
  --trusted-host <hostname>   Mark this host or host:port pair as trusted, even though it does not have valid or any HTTPS.
  --cert <path>               Path to PEM-encoded CA certificate bundle. If provided, overrides the default. See 'SSL Certificate
                              Verification' in pip documentation for more information.
  --client-cert <path>        Path to SSL client certificate, a single file containing the private key and the certificate in PEM
                              format.
  --cache-dir <dir>           Store the cache data in <dir>.
  --no-cache-dir              Disable the cache.
  --disable-pip-version-check
                              Don't periodically check PyPI to determine whether a new version of pip is available for download.
                              Implied with --no-index.
  --no-color                  Suppress colored output.
  --no-python-version-warning
                              Silence deprecation warnings for upcoming unsupported Pythons.
  --use-feature <feature>     Enable new functionality, that may be backward incompatible.
  --use-deprecated <feature>  Enable deprecated functionality, that will be removed in the future.

  
harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ python3 --version
Python 3.10.12

harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ pip -v

Usage:   
  pip <command> [options]

Commands:
  install                     Install packages.
  download                    Download packages.
  uninstall                   Uninstall packages.
  freeze                      Output installed packages in requirements format.
  list                        List installed packages.
  show                        Show information about installed packages.
  check                       Verify installed packages have compatible dependencies.
  config                      Manage local and global configuration.
  search                      Search PyPI for packages.
  cache                       Inspect and manage pip's wheel cache.
  index                       Inspect information available from package indexes.
  wheel                       Build wheels from your requirements.
  hash                        Compute hashes of package archives.
  completion                  A helper command used for command completion.
  debug                       Show information useful for debugging.
  help                        Show help for commands.

General Options:
  -h, --help                  Show help.
  --debug                     Let unhandled exceptions propagate outside the main subroutine, instead of logging them to stderr.
  --isolated                  Run pip in an isolated mode, ignoring environment variables and user configuration.
  --require-virtualenv        Allow pip to only run in a virtual environment; exit with an error otherwise.
  -v, --verbose               Give more output. Option is additive, and can be used up to 3 times.
  -V, --version               Show version and exit.
  -q, --quiet                 Give less output. Option is additive, and can be used up to 3 times (corresponding to WARNING,
                              ERROR, and CRITICAL logging levels).
  --log <path>                Path to a verbose appending log.
  --no-input                  Disable prompting for input.
  --proxy <proxy>             Specify a proxy in the form [user:passwd@]proxy.server:port.
  --retries <retries>         Maximum number of retries each connection should attempt (default 5 times).
  --timeout <sec>             Set the socket timeout (default 15 seconds).
  --exists-action <action>    Default action when a path already exists: (s)witch, (i)gnore, (w)ipe, (b)ackup, (a)bort.
  --trusted-host <hostname>   Mark this host or host:port pair as trusted, even though it does not have valid or any HTTPS.
  --cert <path>               Path to PEM-encoded CA certificate bundle. If provided, overrides the default. See 'SSL Certificate
                              Verification' in pip documentation for more information.
  --client-cert <path>        Path to SSL client certificate, a single file containing the private key and the certificate in PEM
                              format.
  --cache-dir <dir>           Store the cache data in <dir>.
  --no-cache-dir              Disable the cache.
  --disable-pip-version-check
                              Don't periodically check PyPI to determine whether a new version of pip is available for download.
                              Implied with --no-index.
  --no-color                  Suppress colored output.
  --no-python-version-warning
                              Silence deprecation warnings for upcoming unsupported Pythons.
  --use-feature <feature>     Enable new functionality, that may be backward incompatible.
  --use-deprecated <feature>  Enable deprecated functionality, that will be removed in the future.

harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ pip --version
pip 22.0.2 from /usr/lib/python3/dist-packages/pip (python 3.10)


harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ sudo pip3 install virtualenvwrapper

Requirement already satisfied: virtualenvwrapper in /usr/local/lib/python3.10/dist-packages (4.8.4)
Requirement already satisfied: virtualenv in /usr/local/lib/python3.10/dist-packages (from virtualenvwrapper) (20.24.6)
Requirement already satisfied: virtualenv-clone in /usr/local/lib/python3.10/dist-packages (from virtualenvwrapper) (0.5.7)
Requirement already satisfied: stevedore in /usr/local/lib/python3.10/dist-packages (from virtualenvwrapper) (5.1.0)
Requirement already satisfied: pbr!=2.1.0,>=2.0.0 in /usr/local/lib/python3.10/dist-packages (from stevedore->virtualenvwrapper) (6.0.0)
Requirement already satisfied: filelock<4,>=3.12.2 in /usr/local/lib/python3.10/dist-packages (from virtualenv->virtualenvwrapper) (3.13.1)
Requirement already satisfied: distlib<1,>=0.3.7 in /usr/local/lib/python3.10/dist-packages (from virtualenv->virtualenvwrapper) (0.3.7)
Requirement already satisfied: platformdirs<4,>=3.9.1 in /usr/local/lib/python3.10/dist-packages (from virtualenv->virtualenvwrapper) (3.11.0)
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv

harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ export WORKON_HOME = $HOME/.virtualenvs
bash: export: `=': not a valid identifier
bash: export: `/home/harikrishna/.virtualenvs': not a valid identifier


harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ pip install django~=4.2

Defaulting to user installation because normal site-packages is not writeable
Requirement already satisfied: django~=4.2 in /home/harikrishna/.local/lib/python3.10/site-packages (4.2.7)
Requirement already satisfied: asgiref<4,>=3.6.0 in /home/harikrishna/.local/lib/python3.10/site-packages (from django~=4.2) (3.7.2)
Requirement already satisfied: sqlparse>=0.3.1 in /home/harikrishna/.local/lib/python3.10/site-packages (from django~=4.2) (0.4.4)
Requirement already satisfied: typing-extensions>=4 in /home/harikrishna/.local/lib/python3.10/site-packages (from asgiref<4,>=3.6.0->django~=4.2) (4.8.0)

harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ python3 -m django --version

4.2.7

harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ mkdir django_test

harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning$ cd django_test/

harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning/django_test$ django-admin startproject newsite
harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning/django_test$ cd newsite/
harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning/django_test/newsite$ python3 manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).

You have 18 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.
Error: That port is already in use.
harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning/django_test/newsite$ python3 manage.py migrate
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying auth.0012_alter_user_first_name_max_length... OK
  Applying sessions.0001_initial... OK
harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning/django_test/newsite$ python3 manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
Error: That port is already in use.
harikrishna@harikrishna-Lenovo-V330-14IKB:~/Hari_Learning/python_learning/django_test/newsite$ python3 manage.py runserver
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
November 16, 2023 - 09:04:45
Django version 4.2.7, using settings 'newsite.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.

[16/Nov/2023 09:04:55] "GET / HTTP/1.1" 200 10664
