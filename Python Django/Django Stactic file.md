# Django Static Files and Media 
***
## Managing static files (CSS, JavaScript) in Django
Static Files such as Images, CSS, or JS files are often loaded via a different app in production websites to avoid loading multiple stuff from the same server.Let’s understand how to add the Django static file in Django so create a new project first For that let’s create a virtual environment and then download the package if you haven’t downloaded it.

So for that create a Django project "checkstatic" and app named "showstactic" and those app into settings.py as follows..

    django-admin startproject checkstatic
    cd checkstatic
    
    <!-- creating djangoapp -->
    
    python3 manage.py startapp showstatic

settings.py

    INSTALLED_APPS = [
    'showstatic.apps.ShowstaticConfig',
    'django.contrib.admin',
    'django.contrib.auth,
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    ]

Run the server 

    python3 manage.py runserver

### To add static Files

Now we create a static folder inside the main folder (checkstatic) where we will keep our static files. One can add your files (pdf, images, text files, or anything you want)  in the static folder.

Websites generally need to serve additional files such as images, JavaScript, or CSS. In Django, we refer to these files as “static files”. Django provides django.contrib.staticfiles to help you manage them.

### Configuring Static files

* Make sure that django.contrib.staticfiles is included in your INSTALLED_APPS.

* In your settings file, define STATIC_URL, for example:

        STATIC_URL = "static/"
* In your templates, use the static template tag to build the URL for the given relative path using the configured staticfiles STORAGES alias.

        {% load static %}
        <img src="{% static 'my_app/example.jpg' %}" alt="My image">

* Store your static files in a folder called static in your app. For example __showstatic/static/showstatic/example.jpg__.

* In addition to these configuration steps, you’ll also need to actually serve the static files.

* During development, if you use django.contrib.staticfiles, this will be done automatically by runserver when DEBUG is set to True (see django.contrib.staticfiles.views.serve()).

* This method is grossly inefficient and probably insecure, so it is unsuitable for production.

Your project will probably also have static assets that aren’t tied to a particular app. In addition to using a static/ directory inside your apps, you can define a list of directories (STATICFILES_DIRS) in your settings file where Django will also look for static files. For example:

    STATICFILES_DIRS = [
        BASE_DIR / "static",
        "/var/www/static/",
    ]

##### Static file namespacing
Now we might be able to get away with putting our static files directly in my_app/static/ (rather than creating another my_app subdirectory), but it would actually be a bad idea.

 Django will use the first static file it finds whose name matches, and if you had a static file with the same name in a different application, Django would be unable to distinguish between them. We need to be able to point Django at the right one, and the best way to ensure this is by namespacing them. That is, by putting those static files inside another directory named for the application itself.

 #### Serving static files during development

If you use __django.contrib.staticfiles__ as explained above, runserver will do this automatically when DEBUG is set to True. If you don’t have __django.contrib.staticfiles__ in INSTALLED_APPS, you can still manually serve static files using the __django.views.static.serve()__ view.

This is not suitable for production use! 

For example, if your __STATIC_URL__ is defined as static/, you can do this by adding the following snippet to your urls.py:

    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = [
        # ... the rest of your URLconf goes here ...
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

#### Serving files uploaded by a user during development
During development, you can serve user-uploaded media files from __MEDIA_ROOT__ using the django.__views.static.serve()__ view.

This is not suitable for production use! .
For example, if your MEDIA_URL is defined as media/, you can do this by adding the following snippet to your __ROOT_URLCONF__:

    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = [
        # ... URLconf goes here ...
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


### How to Load and use Static Files in Django ?
Now to check just create the “templates” folder in showstatic and create a file name home.html to view our static files

    {% load static %}
    <img src = "{% static 'logo.png' %}" height="200" width="200" class="d-inline-block align-top">
    <br>
    <h1>Hi its working</h1>
Now to view this page we need to give a route for it so now just add this in url.py of checkstatic

    from django.contrib import admin
    from django.urls import path
    from showstatic import views
    urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home,name='home'),
    ]
 And in views.py of showstatic add this 

    def home(request):
    return render(request,'home.html')
Now run the server and see

    python3 manage.py runserver

### Deployment

#### Serving static files in production
During development we use Django and the Django development web server to serve both our dynamic HTML and our static files (CSS, JavaScript, etc.). This is inefficient for static files, because the requests have to pass through Django even though Django doesn't do anything with them. While this doesn't matter during development, it would have a significant performance impact if we were to use the same approach in production.

In the production environment we typically separate the static files from the Django web application, making it easier to serve them directly from the web server or from a content delivery network (CDN).

The important setting variables are:

* __STATIC_URL__: This is the base URL location from which static files will be served, for example on a CDN.
* __STATIC_ROOT__: This is the absolute path to a directory where Django's collectstatic tool will gather any static files referenced in our templates. Once collected, these can then be uploaded as a group to wherever the files are to be hosted.
* __STATICFILES_DIRS__: This lists additional directories that Django's collectstatic tool should search for static files.
Django templates refer to static file locations relative to a static tag, which in turn maps to the STATIC_URL setting. Static files can therefore be uploaded to any host and you can update your application to find them using this setting.

* The collectstatic tool is used to collect static files into the folder defined by the STATIC_ROOT project setting. It is called with the following command:

        python3 manage.py collectstatic
