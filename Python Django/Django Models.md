# Django models
***

## Defining Models and Database Tables


A model is the single, definitive source of information about your data. It contains the essential fields and behaviors of the data you’re storing. Generally, each model maps to a single database table.

The basics:

* Each model is a Python class that subclasses django.db.models.Model.
* Each attribute of the model represents a database field.
* With all of this, Django gives you an automatically-generated database-access API; see Making queries.


        from django.db import models
        from django.urls import reverse

        class MyModelName(models.Model):
        """A typical class defining a model, derived from the Model class."""

        # Fields
                my_field_name = models.CharField(max_length=20, help_text='Enter field documentation')
        # …

        # Metadata
        class Meta:
                ordering = ['-my_field_name']

        # Methods
        def get_absolute_url(self):
                """Returns the URL to access a particular instance of MyModelName."""
                return reverse('model-detail-view', args=[str(self.id)])

        def __str__(self):
                """String for representing the MyModelName object (in Admin site etc.)."""
                return self.my_field_name


### Fields

A model can have an arbitrary number of fields, of any type — each one represents a column of data that we want to store in one of our database tables. Each database record (row) will consist of one of each field value.

        my_field_name = models.CharField(max_length=20, help_text='Enter field documentation')

* Our above example has a single field called my_field_name, of type models.CharField — which means that this field will contain strings of alphanumeric characters. 

* The field types are assigned using specific classes, which determine the type of record that is used to store the data in the database, along with validation criteria to be used when values are received from an HTML form (i.e. what constitutes a valid value). 
* The field types can also take arguments that further specify how the field is stored or can be used. In this case we are giving our field two arguments:

  * max_length=20 — States that the maximum length of a value in this field is 20 characters.
  * help_text='Enter field documentation' — helpful text that may be displayed in a form to help users understand how the field is used.

* Fields also have a label, which is specified using the verbose_name argument (with a default value of None). If verbose_name is not set, the label is created from the field name by replacing any underscores with a space, and capitalizing the first letter (for example, the field my_field_name would have a default label of My field name when used in forms).

* The order that fields are declared will affect their default order if a model is rendered in a form (e.g. in the Admin site), though this may be overridden.

#### COMMON FIELD ARGUMENTS
The following common arguments can be used when declaring many/most of the different field types:

* help_text: Provides a text label for HTML forms (e.g. in the admin site), as described above.
* verbose_name: A human-readable name for the field used in field labels. If not specified, Django will infer the default verbose name from the field name.
* default: The default value for the field. This can be a value or a callable object, in which case the object will be called every time a new record is created.
* null: If True, Django will store blank values as NULL in the database for fields where this is appropriate (a CharField will instead store an empty string). The default is False.
* blank: If True, the field is allowed to be blank in your forms. The default is False, which means that Django's form validation will force you to enter a value. This is often used with null=True, because if you're going to allow blank values, you also want the database to be able to represent them appropriately.
* choices: A group of choices for this field. If this is provided, the default corresponding form widget will be a select box with these choices instead of the standard text field.
* unique: If True, ensures that the field value is unique across the database. This can be used to prevent duplication of fields that can't have the same values. The default is False.
* primary_key: If True, sets the current field as the primary key for the model (A primary key is a special database column designated to uniquely identify all the different table records). If no field is specified as the primary key, Django will automatically add a field for this purpose. The type of auto-created primary key fields can be specified for each app in AppConfig.default_auto_field or globally in the DEFAULT_AUTO_FIELD setting.

#### COMMON FIELD TYPES
The following list describes some of the more commonly used types of fields.

* CharField is used to define short-to-mid sized fixed-length strings. You must specify the max_length of the data to be stored.
* TextField is used for large arbitrary-length strings. You may specify a max_length for the field, but this is used only when the field is displayed in forms (it is not enforced at the database level).
* IntegerField is a field for storing integer (whole number) values, and for validating entered values as integers in forms.
* DateField and DateTimeField are used for storing/representing dates and date/time information (as Python datetime.date and datetime.datetime objects, respectively). These fields can additionally declare the (mutually exclusive) parameters auto_now=True (to set the field to the current date every time the model is saved), auto_now_add (to only set the date when the model is first created), and default (to set a default date that can be overridden by the user).
* EmailField is used to store and validate email addresses.
* FileField and ImageField are used to upload files and images respectively (the ImageField adds additional validation that the uploaded file is an image). These have parameters to define how and where the uploaded files are stored.
* AutoField is a special type of IntegerField that automatically increments. A primary key of this type is automatically added to your model if you don't explicitly specify one.
* ForeignKey is used to specify a one-to-many relationship to another database model (e.g. a car has one manufacturer, but a manufacturer can make many cars). The "one" side of the relationship is the model that contains the "key" (models containing a "foreign key" referring to that "key", are on the "many" side of such a relationship).
* ManyToManyField is used to specify a many-to-many relationship (e.g. a book can have several genres, and each genre can contain several books). In our library app we will use these very similarly to ForeignKeys, but they can be used in more complicated ways to describe the relationships between groups. 

#### Metadata
You can declare model-level metadata for your Model by declaring class Meta, as shown.



        class Meta:
            ordering = ['-my_field_name']

Meta class is an inner class in Django models. Which contain Meta options(metadata) that are used to change the behavior of your model fields like changing order options, whether the model is abstract or not, singular and plural versions of the name etc.

##### Ordering 
Ordering is basically is used to change the order of your model fields.

Order by your_field ascending

        ordering = [‘your_field’]
Order by your_field descending

        ordering = [‘-your_field’]
Ordering two or many fields

        ordering = [‘-your_field’, ‘your_field2’, ]
Order using query expressions

        from django.db.models import F


        ordering = [F('your_field').asc(nulls_last=True)]

So as an example, if we chose to sort books like this by default:


        ordering = ['title', '-pubdate']

the books would be sorted alphabetically by title, from A-Z, and then by publication date inside each title, from newest to oldest.

#### Methods
A model can also have methods.

Minimally, in every model you should define the standard Python class method __str__() to return a human-readable string for each object. This string is used to represent individual records in the administration site (and anywhere else you need to refer to a model instance). Often this will return a title or name field from the model.

        def __str__(self):
                return self.my_field_name

Another common method to include in Django models is get_absolute_url(), which returns a URL for displaying individual model records on the website (if you define this method then Django will automatically add a "View on Site" button to the model's record editing screens in the Admin site). 

        def get_absolute_url(self):
        """Returns the URL to access a particular instance of the model."""
                return reverse('model-detail-view', args=[str(self.id)])

### Activating Models
That small bit of model code gives Django a lot of information. With it, Django is able to:

* Create a database schema (CREATE TABLE statements) for this app.
* Create a Python database-access API for accessing Question and Choice objects.

To include the app in our project, we need to add a reference to its configuration class in the INSTALLED_APPS setting. 

The PollsConfig class is in the polls/apps.py file, so its dotted path is 'polls.apps.PollsConfig'. Edit the mysite/settings.py file and add that dotted path to the INSTALLED_APPS setting. It’ll look like this:

mysite/settings.py
        
        INSTALLED_APPS = [
            "polls.apps.PollsConfig",
            "django.contrib.admin",
            "django.contrib.auth",
            "django.contrib.contenttypes",
            "django.contrib.sessions",
            "django.contrib.messages",
            "django.contrib.staticfiles",
        ]

#### Performing Database Migrations

Migrations are how Django stores changes to your models (and thus your database schema) - they’re files on disk. You can read the migration for your new model if you like; it’s the file polls/migrations/0001_initial.py. Don’t worry, you’re not expected to read them every time Django makes one, but they’re designed to be human-editable in case you want to manually tweak how Django changes things.

        python manage.py makemigrations polls
You should see something similar to the following:

        Migrations for 'polls':
        polls/migrations/0001_initial.py
            - Create model Question
            - Create model Choice

Django uses migrations to track changes to your models and apply those changes to the database. The makemigrations command creates migration files based on your model changes.

        
        python manage.py makemigrations
This command analyzes your models and generates migration files in the migrations directory of your app.

* Apply Migrations:
The migrate command applies the generated migrations to create or update the database tables.


        python manage.py migrate
This command creates the actual database tables based on your models. The database schema is now synchronized with your models.

* Explore the Database:
You can interact with your models and database using the Django shell. It provides a convenient way to test and work with your models.



        python manage.py shell
In the shell, you can create instances of your models, save them to the database, and perform 

### Querying the database using the Django ORM (Object-Relational Mapping).
 If you have a Django model named Question with fields question_text and pub_date,

        from django.db import models

        class Question(models.Model):
                question_text = models.CharField(max_length=200)
                pub_date = models.DateTimeField('date published')

        def __str__(self):
                return self.question_text

Django provides a powerful and expressive ORM that allows you to interact with your database using Python code rather than raw SQL queries.

* Filtering data:


        # Get all questions
        all_questions = Question.objects.all()

        # Get questions published after a specific date
        specific_date = datetime.date(2023, 1, 1)
        recent_questions = Question.objects.filter(pub_date__gt=specific_date)

        # Get questions containing a specific word in the question_text
        keyword = "Django"
        django_questions = Question.objects.filter(question_text__icontains=keyword)
* Chaining filters:

        # Get questions published after a specific date and containing a keyword
        filtered_questions = Question.objects.filter(pub_date__gt=specific_date, question_text__icontains=keyword)
* Get a single object:


        # Get a single question by its ID
        question_id = 1
        single_question = Question.objects.get(pk=question_id)

* Query using related fields:

If you have a ForeignKey relationship, you can use it to query related objects.

        # Get all questions for a specific related object (assuming ForeignKey named 'related_model')
        related_model_id = 1
        related_questions = Question.objects.filter(related_model__id=related_model_id)
* Ordering results:


        # Get questions ordered by publication date in descending order
        ordered_questions = Question.objects.order_by('-pub_date')
* Counting objects:

        # Get the number of questions that match a certain condition
        count_recent_questions = Question.objects.filter(pub_date__gt=specific_date).count()



