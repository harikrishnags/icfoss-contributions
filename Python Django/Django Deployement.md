# Django Deployment
***

There are many options for deploying your Django application, based on your architecture or your particular business needs, but that discussion is outside the scope of what Django can give you as guidance.

Django, being a web framework, needs a web server in order to operate. And since most web servers don’t natively speak Python, we need an interface to make that communication happen.

Django currently supports two interfaces: WSGI and ASGI.

* __WSGI__ is the main Python standard for communicating between web servers and applications, but it only supports synchronous code.
* __ASGI__ is the new, asynchronous-friendly standard that will allow your Django site to use asynchronous Python features, and asynchronous Django features as they are developed.
You should also consider how you will handle static files for your application, and how to handle error reporting.

Finally, before you deploy your application to production, you should run through our deployment checklist to ensure that your configurations are suitable.
## Deployment Checklist
***
The internet is a hostile environment. Before deploying your Django project, you should take some time to review your settings, with security, performance, and operations in mind.

Django includes many security features. Some are built-in and always enabled. Others are optional because they aren’t always appropriate, or because they’re inconvenient for development. For example, forcing HTTPS may not be suitable for all websites, and it’s impractical for local development.

Performance optimizations are another category of trade-offs with convenience. For instance, caching is useful in production, less so for local development. Error reporting needs are also widely different.

The following checklist includes settings that must be set properly for Django to provide the expected level of security are expected to be different in each environment
* enable optional security features;
* enable performance optimizations;
* provide error reporting.

Many of these settings are sensitive and should be treated as confidential. If you’re releasing the source code for your project, a common practice is to publish suitable settings for development, and to use a private settings module for production.

* __Run manage.py check --deploy__  # to know all the deatiled checklist

### Critical settings
***
 __1 .SECRET_KEY__

* The secret key must be a large random value and it must be kept secret.

* Make sure that the key used in production isn’t used anywhere else and avoid committing it to source control. This reduces the number of vectors from which an attacker may acquire the key.

* Instead of hardcoding the secret key in your settings module, consider loading it from an environment variable:

            import os

            SECRET_KEY = os.environ["SECRET_KEY"]
            
    or from a file:

            with open("/etc/secret_key.txt") as f:
                SECRET_KEY = f.read().strip()
* If rotating secret keys, you may use SECRET_KEY_FALLBACKS:

        import os

        SECRET_KEY = os.environ["CURRENT_SECRET_KEY"]
        SECRET_KEY_FALLBACKS = [
            os.environ["OLD_SECRET_KEY"],
        ]
* Ensure that old secret keys are removed from SECRET_KEY_FALLBACKS in a timely manner.

__2.DEBUG__

* You must never enable debug in production.

* You’re certainly developing your project with DEBUG = True, since this enables handy features like full tracebacks in your browser.

* For a production environment, though, this is a really bad idea, because it leaks lots of information about your project: excerpts of your source code, local variables, settings, libraries used, etc.

### Environment-specific settings
***

__3. ALLOWED_HOSTS__

* When DEBUG = False, Django doesn’t work at all without a suitable value for ALLOWED_HOSTS.

* This setting is required to protect your site against some CSRF attacks. If you use a wildcard, you must perform your own validation of the Host HTTP header, or otherwise ensure that you aren’t vulnerable to this category of attacks.

You should also configure the web server that sits in front of Django to validate the host. It should respond with a static error page or ignore requests for incorrect hosts instead of forwarding the request to Django. This way you’ll avoid spurious errors in your Django logs (or emails if you have error reporting configured that way). 

For example, on nginx you might set up a default server to return “444 No Response” on an unrecognized host:

    server {
        listen 80 default_server;
        return 444;
    }
__4.CACHES__

* If you’re using a cache, connection parameters may be different in development and in production. Django defaults to per-process local-memory caching which may not be desirable.

* Cache servers often have weak authentication. Make sure they only accept connections from your application servers.

__5. DATABASES__

* Database connection parameters are probably different in development and in production.

* Database passwords are very sensitive. You should protect them exactly like SECRET_KEY.

* For maximum security, make sure database servers only accept connections from your application servers.



__6.EMAIL_BACKEND and related settings__

* If your site sends emails, these values need to be set correctly.

* By default, Django sends email from webmaster@localhost and root@localhost. However, some mail providers reject email from these addresses. To use different sender addresses, modify the __DEFAULT_FROM_EMAIL__ and __SERVER_EMAIL__ settings.

__7. STATIC_ROOT and STATIC_URL__
* Static files are automatically served by the development server. In production, you must define a STATIC_ROOT directory where __collectstatic__ will copy them.



__8.MEDIA_ROOT and MEDIA_URL__
* Media files are uploaded by your users. They’re untrusted! Make sure your web server never attempts to interpret them. For instance, if a user uploads a .php file, the web server shouldn’t execute it.

__9.HTTPS__
* Any website which allows users to log in should enforce site-wide HTTPS to avoid transmitting access tokens in clear. In Django, access tokens include the login/password, the session cookie, and password reset tokens. (You can’t do much to protect password reset tokens if you’re sending them by email.)

* Protecting sensitive areas such as the user account or the admin isn’t sufficient, because the same session cookie is used for HTTP and HTTPS. Your web server must redirect all HTTP traffic to HTTPS, and only transmit HTTPS requests to Django.

* Once you’ve set up HTTPS, enable the following settings.

* __CSRF_COOKIE_SECURE__

    * Set this to True to avoid transmitting the CSRF cookie over HTTP accidentally.

* __SESSION_COOKIE_SECURE__
    * Set this to True to avoid transmitting the session cookie over HTTP accidentally.

__10.Performance optimizations__

* Setting DEBUG = False disables several features that are only useful in development. In addition, you can tune the following settings.

__11.Sessions__
* Consider using cached sessions to improve performance.

* If using database-backed sessions, regularly clear old sessions to avoid storing unnecessary data.

__12.CONN_MAX_AGE__
* Enabling persistent database connections can result in a nice speed-up when connecting to the database accounts for a significant part of the request processing time.

* This helps a lot on virtualized hosts with limited network performance.

__13.TEMPLATES__

* Enabling the cached template loader often improves performance drastically, as it avoids compiling each template every time it needs to be rendered. When DEBUG = False, the cached template loader is enabled automatically. 

__14.Error reporting__

* By the time you push your code to production, it’s hopefully robust, but you can’t rule out unexpected errors. Thankfully, Django can capture errors and notify you accordingly.

__15.LOGGING__
* Review your logging configuration before putting your website in production, and check that it works as expected as soon as you have received some traffic.


__16.ADMINS and MANAGERS__

* ADMINS will be notified of 500 errors by email.

* MANAGERS will be notified of 404 errors. IGNORABLE_404_URLS can help filter out spurious reports.


* Error reporting by email doesn’t scale very well

* Consider using an error monitoring system such as Sentry before your inbox is flooded by reports. Sentry can also aggregate logs.

__17. Customize the default error views__

* Django includes default views and templates for several HTTP error codes. 

* You may want to override the default templates by creating the following templates in your root template directory: 404.html, 500.html, 403.html, and 400.html. 

* The default error views that use these templates should suffice for 99% of web applications, but you can customize them as well.

### Deploying a Django application to a web server (Heroku)

Deploying in Heroku includes several process
* Install Required Tools
* Create Required Files
* Create a Heroku app
* Edit settings.py
* Make changes for static files

#### Install the required tools
For deploying to Heroku you need to have Heroku CLI (Command Line Interface) installed.

You can do this by going here: https://devcenter.heroku.com/articles/heroku-cli

CLI is required because it will enable us to use features like login, run migrations etc.

#### Create Files required by Heroku

After installing the CLI let’s create all the files that Heroku needs.

__Files are :__

__1.requirements.txt__

Requirements.txt is the simplest to make. Just run the command

        pip freeze > requirements.txt
* This command will make a .txt file which will contain all the packages that are required by your current Django Application.

* If you add any package further then run this command again, this way the file will be updated with the new packages

* What is the use of requirements.txt?

    As you can see that it contains all the dependencies that your app requires. So when you put your app on Heroku it will tell Heroku which packages to install.

#### 2. Procfile
After this make a new file name Procfile and do not put any extension in it. It is a file required by Heroku

According to Heroku :

_Heroku apps include a Procfile that specifies the commands that are executed by the app on startup. You can use a Procfile to declare a variety of process types, including:_

* Your app’s web server
* Multiple types of worker processes
* A singleton process, such as a clock
* Tasks to run before a new release is deployed

For our app we can write the following command

    web: gunicorn name_of_your_app.wsgi -log-file -
If you are confused about your app name, then just go to wsgi.py file in your project and you will find your app name there.

For this, you should have gunicorn installed and added to you requirements.txt file


    pip install gunicorn
#### 3. runtime.txt
After this make a new text file called runtime.txt and inside it write the python version you are using in the following format

    python-3.8.1 
That’s all the files we require. 

Now we have to start editing our settings.py file.

#### Create a Heroku App
This is a simple step and can be done in 2 ways, either by command line or through the Heroku Website.

Use the Heroku Website for now.

* After making Heroku Account you will see an option to create a new app
* It will ask you for a name, the name should be unique. After hit and trials, you will be redirected to your app dashboard.
* Go to the settings tab and there click on Reveal Config Vars
* In the KEY write SECRET_KEY and in VALUE paste the secret key from the settings file and you can change it because only this key will be used.

#### Edit settings.py
There are quite a few changes that should be made in this file.

    DEBUG = False

In the allowed hosts enter the domain of your Heroku app

    ALLOWED_HOSTS = ["your_app_name.herokuapp.com", "127.0.0.1"]

Replace the SECRET_KEY variable with the following (assuming that you have setup the secret key in heroku from the previous step)

    SECRET_KEY = os.environ.get('SECRET_KEY')
What this does is, it gets the SECRET_KEY from the environment. In our case, we can set the secret_key in Heroku and it will provide the key here through environment variables.

#### Setup Static Files
In settings file

    STATIC_URL = '/static/'
Replace this with the following code

    STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
    STATIC_URL = '/static/'

    STATICFILES_DIRS = (
        os.path.join(BASE_DIR, 'static'),
    )
* Basically, this will create a folder named static which will hold all the static files such as CSS files.

* If your App contains images that you have stored on it or the user has the ability to store then add the following lines

        MEDIA_URL = "/media/"
        MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

If you have media files then to allow Django to server them you have to add a line to your urls.

    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = [
        # ... the rest of your URLconf goes here ...
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

For more about static file :

https://docs.djangoproject.com/en/3.1/howto/static-files/

The last thing you need to serve your static files in production is WhiteNoise

With a couple of lines of config WhiteNoise allows your web app to serve its own static files, making it a self-contained unit that can be deployed anywhere without relying on nginx, Amazon S3 or any other external service. (Especially useful on Heroku, OpenShift and other PaaS providers.) 

* Install white noise

    pip install whitenoise
* Add it in MIDDLEWARE in settings.py file

        MIDDLEWARE = [
        # 'django.middleware.security.SecurityMiddleware',
        'whitenoise.middleware.WhiteNoiseMiddleware',
        # ...
        ]
After this don’t forget to run the command which creates the requirements.txt file.

__Whitenoise__ :-
http://whitenoise.evans.io/en/stable/


#### Adding Code To GitHub
* Make a new Github Repo and add all of your code in it.
* Use this post a reference

* After that go to Heroku and under the Deploy tab, you will see an option to connect Github.

* Connect your repo and you can hit the deploy button to deploy your app.

#### Using Heroku Postgres

Basically, all the data you will store will get delete every 24hrs.

To solve Heroku suggest using either AWS or Postgres. Heroku has made it very simple to use Postgres.

Go to your app dashboard and in the Resources section search for Postgres. Select it and you will get something like this


##### Postgres Add-on in Heroku
* Now go to the settings tab and reveal the config vars

* You will see a DATABASE_URL key there. It means that Heroku has added the database and now we have to tell our app to use this database.

* For this, we will be needing another package called dj_database_url. Install it through pip and import it at top of settings.py file

* Now paste the following code below DATABASES in settings file

        db_from_env = dj_database_url.config(conn_max_age=600)
        DATABASES['default'].update(db_from_env)
That’s it now your database is setup

Currently, your database is empty and you might want to fill it.

* Open terminal
type → heroku login
After the login run the following commands
        
        heroku run python manage.py makemigrations
        heroku run python manage.py migrate
        heroku run python manage.py createsuperuser
Now your app is ready to be deployed

either use

    git push heroku master
(after committing the changes) or push it through Github.
