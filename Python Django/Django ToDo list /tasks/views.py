from django.shortcuts import render, redirect
from .models import Task
# Create your views here.

def task_list(request):
    tasks = Task.objects.all()
    return render(request, 'tasks/task_list.html', {'tasks':tasks})

def create_task(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description', '')
        completed = request.POST.get('completed' == 'on' )
        Task.objects.create(title = title, description = description)
        return redirect('task_list')
    return render(request, "tasks/create_task.html")