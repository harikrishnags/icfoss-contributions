from django.db import models

class BooksModel(models.Model):
    title = models.CharField(max_length=255)
    author = models.TextField()

    def __str__(self):
        return f"{self.title} by {self.author}"