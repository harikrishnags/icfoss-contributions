from django import forms
from .models import BooksModel

class BooksForms(forms.ModelForm):
    class Meta:
        model = BooksModel
        fields = ['title', 'author']