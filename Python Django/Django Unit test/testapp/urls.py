from django.urls import path
from .views import add_book, book_list

urlpatterns = [
    path('add_book/', add_book, name='add_book'),
    path('book_list/', book_list, name = 'book_list'),
]