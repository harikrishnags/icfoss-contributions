# myapp/tests.py
from django.test import TestCase
from .models import BooksModel
from .forms import BooksForms
from django.urls import reverse

class BookModelTest(TestCase):
    def test_str_representation(self):
        book = BooksModel(title='The Great Gatsby', author='F. Scott Fitzgerald')
        self.assertEqual(str(book), 'The Great Gatsby by F. Scott Fitzgerald')

class BookFormTest(TestCase):
    def test_valid_form(self):
        form = BooksForms(data={'title': '1984', 'author': 'George Orwell'})
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form = BooksForms(data={'title': '', 'author': 'John Doe'})
        self.assertFalse(form.is_valid())

class AddBookViewTest(TestCase):
    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/add_book/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('add_book'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('add_book'))
        self.assertTemplateUsed(response, 'testapp/add_book.html')

    def test_add_book(self):
        data = {'title': 'The Catcher in the Rye', 'author': 'J.D. Salinger'}
        response = self.client.post(reverse('add_book'), data)
        self.assertEqual(response.status_code, 302)  # 302 indicates a successful redirect
        self.assertEqual(BooksModel.objects.count(), 1)
        self.assertEqual(BooksModel.objects.first().title, 'The Catcher in the Rye')
