from django.shortcuts import render, redirect
from . models import BooksModel
from . forms import BooksForms

# Create your views here.
def add_book(request):
   
    if request.method == "POST":
        form = BooksForms(request.POST)
        if form.is_valid():
            form.save()
            return redirect('book_list')
    else:
        form = BooksForms()
        
           
    return render(request, 'testapp/add_book.html', {'form': form})

def book_list(request):
    books = BooksModel.objects.all()
    return render(request, 'testapp/book_list.html', {'books': books})