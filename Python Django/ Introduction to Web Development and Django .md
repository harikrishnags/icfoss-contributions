# Introduction to Web Development and Django
*** 
## Understanding Web Development Concepts

Web Development refers to overall process of creating websites or web applications, including project designs, layout, coding, content creation and functionality.It involves using a combination of programming languages, tools, and frameworks to bring a website or web application to life.

There are some fundamentals Concepts in the web Development which includes

* Client-side vs Server-Side

    Client-side refers to the user's browser and the technologies running in it. It involves HTML for structure, CSS for styling and Javascript for interactivity. Client-side devolpment is responsible for what user see and interact with their devices.
    
    Server-side is where the application logic, server, and database reside.Server-side devolpment often involves languages like Python(Django), Ruby(Ruby on Rails), PHP, Node.js etc. This side handles data processing, storage and business logic.

* Front-End Development

    This focuses on the user interface and user experience of a website. Front-end developers use technologies like HTML, CSS, and JavaScript to create the visual elements and interactivity that users interact with in their browsers.

   * HTML: Defines webpage structure.
    * CSS: Styles and formats HTML elements.
    * JavaScript: Adds interactivity to web pages.

* Back-End Development
 
    The back end is the server side of a web application, dealing with databases, server configuration, and application logic. Back-end developers use languages like Python, Ruby, Java, or PHP to handle the behind-the-scenes functionality of a website.
    * Server: Responds to client requests.
    * Database: Stores and manages application data.
    * Server-Side Programming: Implements application logic using languages like Python or Ruby.
* HTTP (Hypertext Transfer Protocol)

    HTTP, or Hypertext Transfer Protocol, is an application-layer protocol used for transmitting hypermedia documents, such as HTML. It is the foundation of data communication on the World Wide Web.The protocol used for transferring data on the web. It defines how meassages are formatted and transmitted and how webservers and browsers should responds to various commands.

* RESTful APIs (Representaional State Transfer)

    A Set of principles for designing networked applications. RESTful APIs allow different software systems to communicate over the web. They use standard HTTP methods(GET, POST, PUT,  DELETE) to perform operations.

* Databases 
     
     Relational Databases: Organize datas into tables with relationships.Examples including MySQL and PostgreSQL
     
     Non-Relational Database: Store and retrieve data without using the tabular relationships used in relational databases. e.g. MongoDB
* Web Application Architecture:

    Model View Controller (MVC): A design pattern that sperates the application into three components; Model(Data), View (user interface) and Controller (which handles user input and manages the flow of Data).

    Model View Temaplate in Django : Similar to MVC, Django uses MVT where the template is responsible for generarting HTML .
* Version Control (e.g., Git):

    Helps manage and track changes in the codebase. Git is a popular version control system.
* Web Security:

    Techniques and best practices to protect web applications from security threats, including HTTPS, secure coding practices, and authentication.
* Responsive Design:

    Designing web pages to work well on various devices and screen sizes.
* Web Hosting and Deployment:

    Deploying a web application to a server and making it accessible on the internet. Services like AWS, Heroku, and DigitalOcean provide hosting solutions.

## Overview of Django and its role in web development.
Python Django is a web framework that allows to quickly create efficient web pages. Django is also called batteries included framework because it provides built-in features such as Django Admin Interface, default database – SQLite3, etc. When you’re building a website, you always need a similar set of components: a way to handle user authentication (signing up, signing in, signing out), a management panel for your website, forms, a way to upload files, etc. Django gives you ready-made components to use.
### Django Architecture
Django is based on MVT (Model-View-Template) architecture which has the following three parts 

* Model: The model is going to act as the interface of your data. It is responsible for maintaining data. It is the logical data structure behind the entire application and is represented by a database (generally relational databases such as MySql, Postgres).
* View: The View is the user interface that you see in your browser when you render a website. It is represented by HTML/CSS/Javascript and Jinja files.
* Template: A template consists of static parts of the desired HTML output as well as some special syntax describing how dynamic content will be inserted. 




### Key features of Django include:

* Object-Relational Mapping (ORM): Django provides a powerful ORM system that allows developers to interact with the database using Python objects, making database operations more easy and abstracting away much of the complexity.

* Admin Interface: Django comes with a built-in admin interface that can be easily customized. It provides an easy way to manage the content of your application without having to build a separate admin panel.

* URL Mapping: Django uses a declarative way to map URLs to views, making it easy to manage and understand how different parts of your application connect.

* Template Engine: Django has a template engine that allows developers to create dynamic HTML templates using a syntax that is easy to learn and use.

* Security: Django provides built-in security features to help developers avoid common security mistakes, such as SQL injection, cross-site scripting, and cross-site request forgery.

## Setting up a Django Development Environment

The development environment is an installation of Django on your local computer that you can use for developing and testing Django apps prior to deploying them to a production environment.


##### What operating systems are supported?
Django web applications can be run on almost any machine that can run the Python 3 programming language: Windows, macOS, Linux/Unix, Solaris, to name just a few. Almost any computer should have the necessary performance to run Django during development.
##### What version of Python should be used?
You can use any Python version supported by your target Django release. For Django 4.2 the allowed versions are Python 3.8 to 3.11
##### Where can we download Django?
There are three places to download Django:

* The Python Package Repository (PyPi), using the pip tool. This is the best way to get the latest stable version of Django.
* Use a version from your computer's package manager. Distributions of Django that are bundled with operating systems offer a familiar installation mechanism. Note however that the packaged version may be quite old, and can only be installed into the system Python environment (which may not be what you want).
* Install from source. You can get and install the latest bleeding-edge version of Django from the source. This is not recommended for beginners but is needed when you're ready to start contributing back to Django itself.
##### Which database?
Django officially supports the PostgreSQL, MariaDB, MySQL, Oracle, and SQLite databases, and there are community libraries that provide varying levels of support for other popular SQL and NoSQL databases. 

##### Installing Python 3
In order to use Django you must have Python 3 on your operating system. You will also need the Python Package Index tool * pip3 * which is used to manage (install, update, and remove) Python packages/libraries used by Django and your other Python apps.

        sudo apt install python3-pip    #bash
#### Using Django inside a Virtual Environment

The libraries we'll use for creating our virtual environments are virtualenvwrapper (Linux and macOS) and virtualenvwrapper-win (Windows), which in turn both use the virtualenv tool. The wrapper tools creates a consistent interface for managing interfaces on all platforms.

        sudo pip3 install virtualenvwrapper
Then add the following lines to the end of your shell startup file (this is a hidden file name .bashrc in your home directory). These set the location where the virtual environments should live, the location of your development project directories, and the location of the script installed with this package:

    export WORKON_HOME=$HOME/.virtualenvs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export VIRTUALENVWRAPPER_VIRTUALENV_ARGS=' -p /usr/bin/python3 '
    export PROJECT_HOME=$HOME/Devel
    source /usr/local/bin/virtualenvwrapper.sh

At this point you should see a bunch of scripts being run as shown below:

BASH


        virtualenvwrapper.user_scripts creating /home/ubuntu/.virtualenvs/premkproject
        virtualenvwrapper.user_scripts creating /home/ubuntu/.virtualenvs/postmkproject
        # …
        virtualenvwrapper.user_scripts creating /home/ubuntu/.virtualenvs/preactivate
        virtualenvwrapper.user_scripts creating /home/ubuntu/.virtualenvs/postactivate
        virtualenvwrapper.user_scripts creating /home/ubuntu/.virtualenvs/get_env_details
Now you can create a new virtual environment with the mkvirtualenv command.

        mkvirtualenv my_django_environment


#### Using a virtual environment
There are just a few other useful commands that you should know (there are more in the tool documentation, but these are the ones you'll use regularly):

* deactivate — Exit out of the current Python virtual environment
* workon — List available virtual environments
* workon name_of_environment — Activate the specified Python virtual environment
* rmvirtualenv name_of_environment — Remove the specified environment.

#### Installing Django
Once you've created a virtual environment, and called workon to enter it, you can use pip3 to install Django.

    # Linux/macOS
    python3 -m pip install django~=4.2

 You can test that Django is installed by running the following command (this just tests that Python can find the Django module)

BASH
 
    # Linux/macOS
    python3 -m django --version

#### Other Python tools
Experienced Python developers may install additional tools, such as linters (which help detect common errors in code).

Note that you should use a Django-aware linter such as pylint-django, because some common Python linters (such as pylint) incorrectly report errors in the standard files generated for Django.

To Install

BASH

    pip install pylint-django
pylint-django will not install Django by default because this causes more trouble than good.If you wish to automatically install the latest version of Django then:

    pip install pylint-django[with-django]
### Testing your installation

A more interesting test is to create a skeleton project and see it working. To do this, first navigate in your command prompt/terminal to where you want to store your Django apps. Create a folder for your test site and navigate into it.

BASH


    mkdir django_test
    cd django_test
* You can then create a new skeleton site called "mytestsite" using the django-admin tool as shown. 
* After creating the site you can navigate into the folder where you will find the main script for managing projects, called manage.py.

BASH


    django-admin startproject mytestsite
    cd mytestsite
*  We can run the development web server from within this folder using manage.py and the runserver command, as shown.

BASH



    python3 manage.py runserver



Once the server is running you can view the site by navigating to the following URL on your local web browser: http://127.0.0.1:8000/.
