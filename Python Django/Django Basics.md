# Django Basics
***
## Creating a Django project 

* Use the django-admin tool to generate a project folder, the basic file templates, and manage.py, which serves as your project management script.
* Use manage.py to create one or more applications.
* Register the new applications to include them in the project.
* Hook up the url/path mapper for each application.

##### To create project,

* Open a command shell (or a terminal window), and make sure you are in your virtual environment.
* Navigate to where you want to store your Django apps  and create a folder for your new website . Then change into your newly-created directory

        mkdir django_projects
        cd django_projects
* Create the new project using the django-admin startproject command 

        django-admin startproject newsite
        cd newsite

* The django-admin tool creates a folder/file structure as follows:

        newsite/
            manage.py
            newsite/
                __init__.py
                settings.py
                urls.py
                asgi.py
                wsgi.py
* __init__.py is an empty file that instructs Python to treat this directory as a Python package.
* settings.py contains all the website settings, including registering any applications we create, the location of our static files, database configuration details, etc.
* urls.py defines the site URL-to-view mappings. While this could contain all the URL mapping code, it is more common to delegate some of the mappings to particular applications.
* wsgi.py is used to help your Django application communicate with the web server. You can treat this as boilerplate.
* asgi.py is a standard for Python asynchronous web apps and servers to communicate with each other. Asynchronous Server Gateway Interface (ASGI) is the asynchronous successor to Web Server Gateway Interface (WSGI). ASGI provides a standard for both asynchronous and synchronous Python apps, whereas WSGI provided a standard for synchronous apps only. ASGI is backward-compatible with WSGI and supports multiple servers and application frameworks.
### The development server
The following command will lead to the development server and hence we can know that the Django Project works.
        
        python manage.py runserver

Output on the command line:

Performing system checks...

System check identified no issues (0 silenced).

        You have unapplied migrations; your app may not work properly until they are applied.
        Run 'python manage.py migrate' to apply them.

        November 15, 2023 - 15:50:53
        Django version 4.2, using settings 'mysite.settings'
        Starting development server at http://127.0.0.1:8000/
        Quit the server with CONTROL-C.
### Creating a Django app

* At first make sure that Python Django is installed and then create a Django project.

        django-admin startproject hariproject

* Navigate to your project directory and create a new app:

        cd hariproject
        python manage.py startapp hariapp
* The project structure will be 

        hariproject/           # main project directory
        |-- hariproject/
        |   |-- __init__.py
        |   |-- settings.py     # contains project settings and configuration
        |   |-- urls.py         # Defines project-level URL patterns.
        |   |-- asgi.py
        |   |-- wsgi.py
        |-- hariapp/           # This is the app directory
        |   |-- migrations/
        |   |-- __init__.py
        |   |-- admin.py
        |   |-- apps.py
        |   |-- models.py    #  Defines data models using Django's ORM.
        |   |-- tests.py
        |   |-- views.py     # Defines views, handling HTTP requests and returning responses.
        |-- manage.py

### Introduction to Django models, views, and templates.
Django is based on MVT (Model-View-Template) architecture. MVT is a software design pattern for developing a web application. 

MVT Structure has the following three parts – 

 Model: The model is going to act as the interface of your data. It is responsible for maintaining data. It is the logical data structure behind the entire application and is represented by a database (generally relational databases such as MySql, Postgres)

* Create a model in Django to store data in the database conveniently. Moreover, we can use the admin panel of Django to create, update, delete, or retrieve fields of a model and various similar operations. 
* Django models provide simplicity, consistency, version control, and advanced metadata handling. 

The basics of a model include :

* Each model is a Python class that subclasses django.db.models.Model.
* Each attribute of the model represents a database field. 
* With all of this, Django gives you an automatically generated database-access API

To create Django Models, one needs to have a project and an app working in it. After you start an app you can create models in app/models.py.

##### Create Model in Django
Syntax

        from django.db import models
                
        class ModelName(models.Model):
                field_name = models.Field(**options)

View: The View is the user interface — what you see in your browser when you render a website. It is represented by HTML/CSS/Javascript and Jinja files.

There are two types of views:
* Function-Based Views
* Class-Based Views
##### Function Based Views
Function based views are written using a function in python which receives as an argument HttpRequest object and returns an HttpResponse Object.

 Function based views are generally divided into 4 basic strategies, i.e., CRUD (Create, Retrieve, Update, Delete). CRUD is the base of any framework one is using for development. 

##### Class Based Views
Class-based views provide an alternative way to implement views as Python objects instead of functions. They do not replace function-based views, but have certain differences and advantages when compared to function-based views: 

* Organization of code related to specific HTTP methods (GET, POST, etc.) can be addressed by separate methods instead of conditional branching.
* Object oriented techniques such as mixins (multiple inheritance) can be used to factor code into reusable components.

Class-based views are simpler and efficient to manage than function-based views. A function-based view with tons of lines of code can be converted into class-based views with few lines only. This is where Object-Oriented Programming comes into impact.  

Template: 

A template consists of static parts of the desired HTML output as well as some special syntax describing how dynamic content will be inserted.  

A template in Django is basically written in HTML, CSS, and Javascript in a .html file. Django framework efficiently handles and generates dynamic HTML web pages that are visible to the end-user. Django mainly functions with a backend so, in order to provide a frontend and provide a layout to our website, we use templates.

 There are two methods of adding the template to our website depending on our needs.
We can use a single template directory which will be spread over the entire project. For each app of our project, we can create a different template directory.
#### The Django template language
This is one of the most important facilities provided by Django Templates. A Django template is a text document or a Python string marked-up using the Django template language. 

Some constructs are recognized and interpreted by the template engine. The main ones are variables and tags. The main characteristics of Django Template language are Variables, Tags, Filters, and Comments. 

##### Jinja Variables
Variables output a value from the context, which is a dict-like object mapping keys to values. The context object we sent from the view can be accessed in the template using variables of Django Template. 

Syntax: 
        
        {{ variable_name }}

Variables are surrounded by {{ and }} like this:  

        My first name is {{ first_name }}. My last name is {{ last_name }}. 
With a context of {‘first_name’: ‘Hari’, ‘last_name’: 'krishna'}, this template renders to: 

        My first name is Hari My last name is krishna.
