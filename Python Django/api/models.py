from django.db import models

# Create your models here.
class Item(models.Model):
    category = models.CharField(max_length=250)
    subcategory = models.CharField(max_length=250)
    name = models.CharField(max_length=50)
    amount= models.PositiveIntegerField()

    def __str__(self) -> str:
        return self.name