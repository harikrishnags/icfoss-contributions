from django.db.models import fields
from rest_framework import serializers
from . models import  Item

class ItemSerializer(serializers.ModelSerializer): #  Serializer for the Item model. This serializer defines how the Item model
                                                        # should be serialized when converted to a JSON format. It also handles validation.
    class Meta:
        # Define the Meta class to specify the model and fields to be serialized
        model = Item
        # Specify the fields to be serialized. The order in which the fields are specified
        # also defines the order in which they will appear in the JSON representation.
        fields = ('category', 'subcategory','name', 'amount')