# Django Views
***
A view function is a Python function that takes a Web request and returns a Web response. This response can be the HTML contents of a Web page, or a redirect, or a 404 error, or an XML document, or an image, anything that a web browser can display. 

Django views are part of the user interface — they usually render the HTML/CSS/Javascript in your Template files into what you see in your browser when you render a web page.

###### Django view example

    
    from django.http import HttpResponse       # import Http Response from django
    
    import datetime    # get datetime

    # create a function
    def my_view(request):
        # fetch date and time
        now = datetime.datetime.now()
        # convert to string
        html = "Time is {}".format(now)
        # return response
        return HttpResponse(html)


    Types of Views
Django views are divided into two major categories:-

* Function-Based Views
* Class-Based Views

#### Function-Based Views

Function Based Views
Function based views are written using a function in python which receives as an argument HttpRequest object and returns an HttpResponse Object. Function based views are generally divided into 4 basic strategies, i.e., CRUD (Create, Retrieve, Update, Delete). CRUD is the base of any framework one is using for development. 
 
For Example 

* Create a model 

        from django.db import models

        class hariModel(models.Model):
            title = models.CharField(max_length=100)
            description = models.TextField()

        def __str__(self):
            return self.title
After creating this model, we need to run two commands in order to create Database for the same.

    Python manage.py makemigrations
    Python manage.py migrate
    python3 manage.py migrate --run-syncdb 
Now let’s create some instances of this model using shell, run form bash,

    Python manage.py shell
    Enter following commands
    >>> from myapp.models import hariModel
    >>>hariModel.objects.create(
                        title="title1",
                        description="description1").save()
    >>> hariModel.objects.create(
                        title="title2",
                        description="description2").save()
    >>> hariModel.objects.create(
                        title="title2",
                        description="description2").save()

Now if you want to see your model and its data in the admin panel, then you need to register your model.
Let’s register this model. In apps/admin.py,

    from django.contrib import admin
    from .models import hariModel
    # Register your models here.
    admin.site.register(hariModel)

Let’s create a view and template for the same. In apps/views.py,
    
    from django.shortcuts import render

    from .models import hariModel

    def list_view(request):

        context = {}

        context['dataset'] = hariModel.objects.all()

        return(render, "list_view.html", context)
Create a template in templates/list_view.html,


    <div class="main">

        {% for data in dataset %}.

        {{ data.title }}<br/>
        {{ data.description }}<br/>
        <hr/>

        {% endfor %}
* Run the server

        python3 manage.py runserver
    
### Class Based Views
Class-based views provide an alternative way to implement views as Python objects instead of functions. They do not replace function-based views, but have certain differences and advantages when compared to function-based views: 

Organization of code related to specific HTTP methods (GET, POST, etc.) can be addressed by separate methods instead of conditional branching.

Object oriented techniques such as mixins (multiple inheritance) can be used to factor code into reusable components.

Class-based views are simpler and efficient to manage than function-based views. A function-based view with tons of lines of code can be converted into class-based views with few lines only. This is where Object-Oriented Programming comes into impact.  

from django.views.generic.list import ListView
from .models import GeeksModel

class myapp(ListView):

	# specify the model for list view
	model = hariModel

Now create a URL path to map the view. In myapp/urls.py,


from django.urls import path
    
    # importing views from views..py
    from .views import newList
    urlpatterns = [
        path('', newList.as_view()),
    ]
