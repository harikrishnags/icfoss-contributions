# Django Middleware
***
Middleware is a framework of hooks into Django’s request/response processing. It’s a light, low-level “plugin” system for globally altering Django’s input or output.

Middleware is a series of processing layers present between the web server and the view (controller in some frameworks), allowing the user to intercept, modify, and add behavior to these requests and responses.

In Python Django, middleware is a framework that provides a method to process requests and responses globally before they are processed by the view or after they leave the view. Middleware components are designed so that they remain between the web server and the view, allowing us to perform various operations on requests and responses as they pass through the Django application and web browser.

Each middleware component is responsible for doing some specific function. For example, Django includes a middleware component, AuthenticationMiddleware, that associates users with requests using sessions.
## Types of Middleware in Django
Django’s middleware can be divided into 2 types: built-in and custom.

__Built-in Django Middleware__: Django comes with a set of built-in middleware classes. These are some of the most commonly used default middleware classes in Django:

* SecurityMiddleware: It provides several security-related features such as ensuring that all HTTP requests are redirected to HTTPS and is used to add security headers to responses.
* SessionMiddleware: It helps in managing sessions for users. It enables Django to handle session data, making it available to views and templates.
* CommonMiddleware: Handles common operations such as URL redirection (adding/removing “www” prefix), appending slashes to URLs.
* CsrfViewMiddleware: Enables protection against Cross-Site Request Forgery (CSRF) attacks. It adds and verifies a CSRF token in POST requests to protect against malicious requests from other sites.
* AuthenticationMiddleware: Adds the ‘user' attribute to the request, representing the currently logged-in user, if applicable. It also manages the user’s session and authentication state.
### Hooks

In Django, hooks refer to specific points in the request-response process where you can plug in custom functionality using middleware or signals. These hooks provide developers with the ability to modify, augment, or execute code at various stages of handling an HTTP request.

#### Middleware Hooks:

* process_request: Executed at the beginning of the request process. It can be used to modify the request before it reaches the view.
* process_view: Called just before Django calls the view. It allows modification of the view function, arguments, or keyword arguments.
* process_template_response: Called when a response has a render() method called on it, allowing modification of the response object before it is returned.
* process_response: Executed on each response after it has been processed. It can be used to modify the response or perform cleanup tasks.

### Middleware Factory 
A middleware factory is a callable (a function or a class) that produces a middleware instance. In Django, a middleware factory is a function or class that takes a get_response argument and returns a middleware function or instance.
##### Middleware factory function
    def my_middleware(get_response):
        # This is the middleware function that will be called for each request.
        def middleware(request):
            # Code to be executed before the view is called.
            response = get_response(request)
            # Code to be executed after the view is called.
            return response
        return middleware

In this example, my_middleware is a middleware factory function. When this function is called with get_response as an argument, it returns a middleware function (middleware) that will be executed for each request.
##### Middleware factory class

    class MyMiddleware:
        def __init__(self, get_response):
            self.get_response = get_response

        def __call__(self, request):
            # Code to be executed before the view is called.
            response = self.get_response(request)
            # Code to be executed after the view is called.
            return response

In this example, MyMiddleware is a middleware factory class. When an instance of this class is created with get_response as an argument, the instance becomes a callable middleware. The __call__ method is executed for each request.

To use a middleware factory in Django, you would include it in the MIDDLEWARE setting in your settings.py file:

 settings.py/

    MIDDLEWARE = [
        # Other middleware classes...
        'path.to.my_middleware',  # for a function-based middleware factory
        'path.to.MyMiddleware',   # for a class-based middleware factory
        # More middleware classes...
    ]
Using middleware factories allows for more dynamic configuration and customization of middleware behavior, especially when you need to pass additional parameters or set up middleware based on runtime conditions.

### Activating middleware
To activate a middleware component, add it to the MIDDLEWARE list in your Django settings.

In MIDDLEWARE, each middleware component is represented by a string: the full Python path to the middleware factory’s class or function name. For example, here’s the default value created by django-admin startproject:

    MIDDLEWARE = [
        "django.middleware.security.SecurityMiddleware",
        "django.contrib.sessions.middleware.SessionMiddleware",
        "django.middleware.common.CommonMiddleware",
        "django.middleware.csrf.CsrfViewMiddleware",
        "django.contrib.auth.middleware.AuthenticationMiddleware",
        "django.contrib.messages.middleware.MessageMiddleware",
        "django.middleware.clickjacking.XFrameOptionsMiddleware",
    ]

The order in MIDDLEWARE matters because a middleware can depend on other middleware. For instance, AuthenticationMiddleware stores the authenticated user in the session; therefore, it must run after SessionMiddleware. See Middleware ordering for some common hints about ordering of Django middleware classes.

### Middleware order and layering
During the request phase, before calling the view, Django applies middleware in the order it’s defined in MIDDLEWARE, top-down.

You can think of it like an onion: each middleware class is a “layer” that wraps the view, which is in the core of the onion. If the request passes through all the layers of the onion (each one calls get_response to pass the request in to the next layer), all the way to the view at the core, the response will then pass through every layer (in reverse order) on the way back out.

If one of the layers decides to short-circuit and return a response without ever calling its get_response, none of the layers of the onion inside that layer (including the view) will see the request or the response. The response will only return through the same layers that the request passed in through.

## Other middleware hooks
Besides the basic request/response middleware pattern described earlier, you can add three other special methods to class-based middleware:

### process_view()
__process_view__(request, view_func, view_args, view_kwargs)

request is an HttpRequest object. view_func is the Python function that Django is about to use. (It’s the actual function object, not the name of the function as a string.) view_args is a list of positional arguments that will be passed to the view, and view_kwargs is a dictionary of keyword arguments that will be passed to the view. Neither view_args nor view_kwargs include the first view argument (request).

__process_view__() is called just before Django calls the view.

It should return either None or an HttpResponse object. If it returns None, Django will continue processing this request, executing any other process_view() middleware and, then, the appropriate view. If it returns an HttpResponse object, Django won’t bother calling the appropriate view; it’ll apply response middleware to that HttpResponse and return the result.

### process_exception()
__process_exception__(request, exception)

request is an HttpRequest object. exception is an Exception object raised by the view function.

Django calls __process_exception__() when a view raises an exception. 

__process_exception__() should return either None or an HttpResponse object. If it returns an HttpResponse object, the template response and response middleware will be applied and the resulting response returned to the browser. Otherwise, default exception handling kicks in.

Again, middleware are run in reverse order during the response phase, which includes process_exception. If an exception middleware returns a response, the process_exception methods of the middleware classes above that middleware won’t be called at all.

### process_template_response()
__process_template_response__(request, response)

request is an HttpRequest object. response is the TemplateResponse object (or equivalent) returned by a Django view or by a middleware.

__process_template_response__() is called just after the view has finished executing, if the response instance has a render() method, indicating that it is a TemplateResponse or equivalent.

It must return a response object that implements a render method. It could alter the given response by changing response.template_name and response.context_data, or it could create and return a brand-new TemplateResponse or equivalent.

You don’t need to explicitly render responses – responses will be automatically rendered once all template response middleware has been called.

Middleware are run in reverse order during the response phase, which includes process_template_response().
