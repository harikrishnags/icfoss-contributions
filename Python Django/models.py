# Example for models

from django.db import models    

class Question(models.Model):
    Question_text = models.CharField(max_length=200)
    published_date = models.DateTimeField("date published")



class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)


# Example-2

  from django.db import models
  from django.urls import reverse

  class MyModelName(models.Model):
  """A typical class defining a model, derived from the Model class."""

  # Fields
  my_field_name = models.CharField(max_length=20, help_text='Enter field documentation')
  # …

  # Metadata
  class Meta:
          ordering = ['-my_field_name']

  # Methods
  def get_absolute_url(self):
          """Returns the URL to access a particular instance of MyModelName."""
          return reverse('model-detail-view', args=[str(self.id)])

  def __str__(self):
          """String for representing the MyModelName object (in Admin site etc.)."""
          return self.my_field_name
