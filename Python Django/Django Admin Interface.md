# Django Admin Interface
***

When you start a project with Django it also provides a default admin interface that can be used to perform create, read, update, and delete operations on the model directly. It reads a set of data that explains and gives information about data from the model, to provide an instant interface where the user can adjust the contents of the application. This is an in-built module designed to execute admin-related work for the user.

## Creating an admin user
First we’ll need to create a user who can login to the admin site. Run the following command:

    python manage.py createsuperuser
Enter your desired username and press enter.

    Username: admin
You will then be prompted for your desired email address:

    Email address: admin@example.com
The final step is to enter your password. You will be asked to enter your password twice, the second time as a confirmation of the first.

    Password: **********
    Password (again): *********
    Superuser created successfully.

Start the development server
The Django admin site is activated by default. Let’s start the development server and explore it.

If the server is not running start it like so:

     python manage.py runserver
Now, open a Web browser and go to “/admin/” on your local domain – e.g., http://127.0.0.1:8000/admin/. You should see the admin’s login screen:

Then If a model is created then the model should be registered in the Django Admin interface.

So that in the app/forms.py/

    from django.contrib import admin

    from .models import yourmodelname

    admin.site.register(yourmodelname)

Let us create a sample project for better understanding of Django admin Interface

Create a django project namely " AdminProject" and also create a Django App namely "Book".

    django-admin startproject AdminProject

    /AdminProject
    python3 manage.py startapp Book

Here we creating a Book application that lists new book, their title, author, published date thus we have to define those into models.

Book/models.py

    from django.db import models

    class Book(models.Model):
        title = models.CharField(max_length=100)
        author = models.CharField(max_length=100)
        publishedDate = models.DateField()
        is_available = models.BooleanField(default=True)

        def str(self):
            return self.title


After creating the models register those models in admin.py to make it accessible in the admin interface.

    from django.contrib  import admin
    from .models import Book

    admin.site.register(Book)

Install the app - "Book" in settings.py in the installed apps and make migrations.

For accessing Django Admin Interface Create a user who can login to the admin site. Run the following command:

    python3 manage.py createsuperuser

And Now run the server 

    python3 manage.py runserver

Visit http://127.0.0.1:8000/admin/ and log in. Now, you should see the customized display and filtering options for the Book model.

### Customizing Model Admin
Customize the appearance and behavior of your models in the admin interface by creating a custom ModelAdmin class and registering it.

Create a BookAdmin class in the book/admin.py file to customize the display and filtering options.
    `
        
        # book/admin.py
        from django.contrib import admin
        from .models import Book

        class BookAdmin(admin.ModelAdmin):
            list_display = ('title', 'author', 'published_date', 'is_available')
            list_filter = ('is_available', 'published_date')
            search_fields = ('title', 'author')

        admin.site.register(Book, BookAdmin)`

Run the development server

    python3 manage.py runserver

Visit http://127.0.0.1:8000/admin/ and log in. Now, you should see the customized display and filtering options for the Book model.

You can explore further customizations such as adding custom actions, overriding templates, and using third-party packages like django-admin-honeypot for additional security.

##### Add Custom Actions

Custom actions allow you to perform bulk actions on selected items in the admin interface. In book/admin.py, add the following to your BookAdmin class:

    # book/admin.py
    from django.contrib import admin
    from .models import Book

    class BookAdmin(admin.ModelAdmin):
        # ... (your existing code)

        actions = ['mark_as_available', 'mark_as_unavailable']

        def mark_as_available(modeladmin, request, queryset):
            queryset.update(is_available=True)
        mark_as_available.short_description = "Mark selected books as available"

        def mark_as_unavailable(modeladmin, request, queryset):
            queryset.update(is_available=False)
        mark_as_unavailable.short_description = "Mark selected books as unavailable"

    admin.site.register(Book, BookAdmin)

Now, in the admin interface, you'll have two new actions: "Mark selected books as available" and "Mark selected books as unavailable."
