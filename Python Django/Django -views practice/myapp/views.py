from django.shortcuts import render

from .models import hariModel

def list_view(request):

    context = {}

    context['dataset'] = hariModel.objects.all()

    return(render, "list_view.html", context)
