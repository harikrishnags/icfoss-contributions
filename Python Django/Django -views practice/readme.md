## Django-views practice - Steps
***
* Created Django project named "newproject"
and created djnago app named "myapp"
* Created a model anamed "hariModel" with 2 fields "title" and "description".

* Created view that retrieves all instances of hariModel from the database and passes them to the template for rendering.
from django.shortcuts import render
from .models import hariModel

        from django.shortcuts import render

        from .models import hariModel

        def list_view(request):

            context = {}

            context['dataset'] = hariModel.objects.all()

            return(render, "list_view.html", context)
        

    * At first Imports the render function from Django, which is used to render HTML templates with a given context and also Imports the  hariModel model from the same Django app.
    * Defines a view function named list_view that takes a request object as a parameter.
    * context = {}: Initializes an empty dictionary called context to store data that will be passed to the template.

   * context['dataset'] = hariModel.objects.all(): Retrieves all instances of the hariModel model from the database and stores them in the dataset key of the context dictionary.

    * return render(request, "list_view.html", context): Calls the render function, passing the request object, the template name ("list_view.html"), and the context dictionary. This will render the template with the provided data and return an HTTP response.
