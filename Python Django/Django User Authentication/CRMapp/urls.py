
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home , name = 'homepage'),
    # path('login/', views.login_view , name = 'loginpage'),
    path('logout/', views.logout_view , name = 'logout')
]
