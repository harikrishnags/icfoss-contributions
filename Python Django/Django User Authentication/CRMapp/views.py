from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

# Create your views here.
def home(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
		# Authenticate
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, "You Have Been Logged In!")
            return redirect('homepage')
        else:
            messages.success(request, "There Was An Error Logging In, Please Try Again...")
            return redirect('homepage')
    else:
         return render (request, 'CRMapp/home.html', {})



def logout_view(request):
    logout(request)
    messages.success(request, "Successfully logged out !")
    return redirect('homepage')