from django.contrib import admin

# Register your models here.
from .models import Book



class BookAdmiin(admin.ModelAdmin):
    list_display = ('title', 'author', 'publishedDate', 'is_available')
    list_filter = ('is_available', 'publishedDate')
    search_fields = ('title', 'author')

admin.site.register(Book, BookAdmiin)