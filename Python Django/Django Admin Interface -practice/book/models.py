from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    publishedDate = models.DateField()
    is_available = models.BooleanField(default=True)

    def __str__(self):
        return self.title